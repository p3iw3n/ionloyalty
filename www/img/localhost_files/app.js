// Ionic Starter App
var apiurl = "http://illigo.sg/client/ion/revamp2016/loyalty_api/";
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngMessages', 'ngCordova', 'starter.services', 'ion-digit-keyboard', 'starter.directives', 'ionic.ion.autoListDivider'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.navBar.alignTitle('right');
    $ionicConfigProvider.views.swipeBackEnabled(false);
    $stateProvider

        .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })

    .state('app.search', {
            url: '/search',
            views: {
                'menuContent': {
                    templateUrl: 'templates/search.html'
                }
            }
        })
        // .state('app.rewards', {
        //   url: '/rewards',
        //   views: {
        //     'menuContent': {
        //       templateUrl: 'templates/rewards.html',
        //       controller:'RewardsCtrl'
        //     }
        //   }
        // })
        .state('app.login', {
            url: '/login',
            views: {
                'menuContent': {
                    templateUrl: 'templates/login.html',
                    controller: 'LoginCtrl'
                }
            }
        })
        .state('app.register', {
            url: '/register',
            views: {
                'menuContent': {
                    templateUrl: 'templates/register.html',
                    controller: 'RegisterCtrl'
                }
            }
        })

    .state('app.edit_profile', {
        url: '/edit_profile',
        views: {
            'menuContent': {
                templateUrl: 'templates/edit_profile.html',
                controller: 'EditProfileCtrl'
            }
        }
    })

    .state('app.forgotPwd', {
            url: '/forgotPwd',
            views: {
                'menuContent': {
                    templateUrl: 'templates/forgotPwd.html',
                    controller: 'ForgotPwdCtrl'
                }
            }
        })
        .state('app.activate', {
            url: '/activate',
            views: {
                'menuContent': {
                    templateUrl: 'templates/activate.html',
                    controller: 'ActivationCtrl'

                }
            }
        })
        .state('app.setpwd', {
            url: '/setpwd',
            views: {
                'menuContent': {
                    templateUrl: 'templates/setpwd.html',
                    controller: 'SetPwdCtrl'

                }
            }
        })

    .state('app.changePwd', {
            url: '/changePwd',
            views: {
                'menuContent': {
                    templateUrl: 'templates/changePwd.html',
                    controller: 'ForgotPwdCtrl'
                }
            }
        })
        .state('app.transactions', {
            url: '/transactions',
            views: {
                'menuContent': {
                    templateUrl: 'templates/transactions.html',
                    controller: 'TransactionCtrl'
                }
            }
        })
        .state('app.scanresults', {
            url: '/scanresults/:id',
            views: {
                'menuContent': {
                    templateUrl: 'templates/scan_results.html',
                    controller: 'ScanResultCtrl'
                }
            }
        })

    .state('app.privileges', {
        url: '/privileges',
        views: {
            'menuContent': {
                templateUrl: 'templates/privileges.html',
                controller: 'PrivilegeCtrl'
            }
        }
    })


    .state('app.single', {
            url: '/playlists/:playlistId',
            views: {
                'menuContent': {
                    templateUrl: 'templates/playlist.html',
                    controller: 'PlaylistCtrl'
                }
            }
        })
        .state('app.dashboard', {
            url: '/dashboard',
            views: {
                'menuContent': {
                    templateUrl: 'templates/dashboard.html',
                    controller: 'DashboardCtrl'
                }
            }
        })
        .state('app.iu_manager', {
            url: '/iu_manager',
            views: {
                'menuContent': {
                    templateUrl: 'templates/iu_manager.html',
                    controller: 'IUManagerCtrl'
                }
            }
        })
        .state('app.privileges_search', {
            url: '/privileges_search',
            views: {
                'menuContent': {
                    templateUrl: 'templates/privileges_search.html',
                    controller: 'PrivilegesSearchCtrl'
                }
            }
        })
        .state('app.total_points', {
            url: '/total_points',
            views: {
                'menuContent': {
                    templateUrl: 'templates/total_points.html',
                    controller: 'TotalPointsCtrl'
                }
            }
        })
        .state('app.carpark_dollar', {
            url: '/carpark_dollar',
            views: {
                'menuContent': {
                    templateUrl: 'templates/carpark_dollar.html',
                    controller: 'CarparkDollarCtrl'
                }
            }
        })
        .state('app.scan_receipts', {
            url: '/scan_receipts',
            views: {
                'menuContent': {
                    templateUrl: 'templates/scan_receipts.html',
                    controller: 'ScanReceiptsCtrl'
                }
            }
        })
        .state('app.ionrewards_tnc', {
            url: '/rewards_tnc',
            views: {
                'menuContent': {
                    templateUrl: 'templates/ionrewards_tnc.html',
                    controller: 'RewardsTncCtrl'
                }
            }
        });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/login');
    // $urlRouterProvider.otherwise('/scan_receipts');
});