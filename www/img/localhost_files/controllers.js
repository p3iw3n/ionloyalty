angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $state, $ionicHistory) {
    $scope.isIOS = ionic.Platform.isIOS();
    // console.log(isIOS);
    $scope.device_type;
    if ($scope.isIOS == true) {
        $scope.device_type = 'ios'
    } else {
        $scope.device_type = 'android'
    }

    // Form data for the login modal
    $scope.loginData = {};



    // Triggered in the login modal to close it
    $scope.closeRewards = function() {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.rewards = function() {
        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/rewards.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
            $scope.modal.show();
        });

    };





    $scope.showRegisterLanding = function() {
        $ionicModal.fromTemplateUrl('templates/register_landing.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.register_landing = modal;
            $scope.register_landing.show();
            // $scope.showRegisterLanding();
        });

    };
    $scope.closeRegisterLanding = function() {
        $scope.register_landing.hide();
    };
    $scope.gototnc = function() {

        $state.go('app.ionrewards_tnc');
    }

    $scope.redirect = function(action) {
        if (action == 'login') {
            $scope.closeRewards();
            $state.go('app.login');
        } else if (action == 'register') {
            $scope.closeRewards();
            $scope.showRegisterLanding();
        } else if (action == 'backLogin') {
            $scope.closeRegisterLanding();
            $scope.rewards();
        } else if (action == 'newMember') {
            $scope.closeRegisterLanding();
            $state.go('app.register');
        } else if (action == 'Dashboard') {
            // $scope.closeRegisterLanding();
            $state.go('app.dashboard');
        } else if (action == 'Privileges') {
            $state.go('app.privileges');
        } else if (action == 'Transactions') {
            $state.go('app.transactions');
        } else if (action == 'Privilege') {
            $state.go('app.privilege');
        } else if (action == 'IUManager') {
            $state.go('app.iu_manager');
        } else if (action == 'ScanReceipts') {
            $state.go('app.scan_receipts');
        } else if (action == 'activate') {
            $scope.closeRegisterLanding();
            $state.go('app.activate');
        } else if (action == 'privileges_search') {
            $state.go('app.privileges_search');
        } else if (action == 'total_points') {
            $state.go('app.total_points');
        } else if (action == 'Carpark') {
            $state.go('app.carpark_dollar');
        }
    }
    $scope.backView = function() {

        $ionicHistory.goBack();
        //$ionicHistory.backView();
    }

    $scope.hideNavView = function() {
        document.getElementById('nav-view').classList.add('hide_nav_view');

        html = document.getElementsByTagName('html');
        if (html.length > 0) {
            html[0].classList.add("hide_nav_view_transparent");
        }
        body = document.getElementsByTagName('body');
        if (body.length > 0) {
            body[0].classList.add("hide_nav_view_transparent");
            body[0].classList.add("show_guide_line");
        }
        ionapp = document.getElementsByTagName('ion-app');
        if (ionapp.length > 0) {
            ionapp[0].classList.add("hide_nav_view_transparent");
        }
        ioncontent = document.getElementsByTagName('ion-content');
        if (ioncontent.length > 0) {
            ioncontent[0].classList.add("hide_nav_view_transparent");
        }
    }

    $scope.showNavView = function() {
        document.getElementById('nav-view').classList.remove('hide_nav_view');

        ioncontent = document.getElementsByTagName('ion-content');
        if (ioncontent.length > 0) {
            ioncontent[0].classList.remove("hide_nav_view_transparent");
        }
        ionapp = document.getElementsByTagName('ion-app');
        if (ionapp.length > 0) {
            ionapp[0].classList.remove("hide_nav_view_transparent");
        }
        body = document.getElementsByTagName('body');
        if (body.length > 0) {
            body[0].classList.remove("show_guide_line");
            body[0].classList.remove("hide_nav_view_transparent");
        }
        html = document.getElementsByTagName('html');
        if (html.length > 0) {
            html[0].classList.remove("hide_nav_view_transparent");
        }
    }
})


.controller('LoginCtrl', function($scope, $stateParams, $state, Login, $ionicPopup) {
    // $scope.loginData = [];
    // $scope.loginData.pwd = '';

    $scope.rewards();

    $scope.keyboardVisible = false;
    $scope.keyboardSettings = {
        theme: 'light',
        showLetters: true,
        action: function(number) {
            // console.log(number);
            $scope.loginData.pwd += number;

        },
        leftButton: {
            html: '',
            style: {
                bgColor: '#d2d6db',
            }
        },
        rightButton: {
            html: '<i class="icon ion-backspace-outline"></i>',
            action: function() {
                $scope.loginData.pwd = $scope.loginData.pwd.slice(0, -1);
            },
            style: {
                bgColor: '#d2d6db',
            }
        }
    }
    $scope.showKeyboard = function($event) {
        // cordova.plugins.Keyboard.close();
        $scope.keyboardVisible = true;
    }
    $scope.hideKeyboard = function($event) {
        $scope.keyboardVisible = false;
    }
    $scope.doLogin = function(form) {
        // console.log($scope.loginData);
        // $scope.hideKeyboard();
        if (!form.$invalid) {

            Login.dologin($scope.loginData, $scope.device_type, function($data, $status, $headers, $config) {
                form.$submitted = false;

                $scope.loginData = [];
                sessionStorage.setItem('username', $scope.loginData.username);

                // cordova.plugins.IonGss.auth($scope.loginData.username);
                $scope.loginData.card_no = $data.data.card_no;
                // console.log($scope.loginData.card_no);
                Login.setProfile($data.data);


                // $scope.loginData.nric = $data.data.nric;
                // $scope.loginData.card_no = $data.data.card_no;
                // $scope.loginData.area_code = $data.data.area_code;
                // $scope.loginData.country_code = $data.data.country_code;
                // $scope.loginData.contact = $data.data.contact;
                // $scope.loginData.family_name = $data.data.family_name;
                // $scope.loginData.given_name = $data.data.given_name;
                // $scope.loginData.dob = $data.data.dob;
                // $scope.loginData.gender = $data.data.gender;
                // $scope.loginData.nationality = $data.data.nationality;
                // $scope.loginData.block = $data.data.block;
                // $scope.loginData.street = $data.data.street;
                // $scope.loginData.level = $data.data.level;
                // $scope.loginData.postal_code = $data.data.postal_code;
                // $scope.loginData.building = $data.data.building;
                // $scope.loginData.country = $data.data.country;

                $scope.redirect('Dashboard');
            }, function($data, $status, $headers, $config, $errormsg) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Error',
                    template: $data.msg,
                    buttons: [{
                        type: 'ion-btn',
                        text: 'OK'
                    }]
                });
            })
        }

    }

})


.controller('RegisterCtrl', function($scope, $stateParams, $state, Register, $ionicPopup) {
        $scope.keyboardVisible = false;
        $scope.registerData = [];
        $scope.registerData.gender = '';
        $scope.registerData.salutation = '';
        $scope.registerData.country_code = '';
        $scope.registerData.nationality = '';
        $scope.registerData.country = '';
        $scope.registerData.sign_up_id = '';
        $scope.registerData.agree = false;
        $scope.registerData.pdpa = [];
        $scope.registerData.pdpa['sms'] = true;
        $scope.registerData.pdpa['post'] = true;
        $scope.registerData.pdpa['email'] = true;
        $scope.registerData.pdpa['call'] = true;

        $scope.salutation = Register.getSalutation();
        Register.retrieveSalutation($scope.device_type, function($data, $status, $headers, $config) {
            $scope.salutation = Register.getSalutation();
        }, function($data, $status, $headers, $config) {

        })
        $scope.nationality = Register.getNationality();
        Register.retrieveNationality($scope.device_type, function($data, $status, $headers, $config) {
            $scope.nationality = Register.getNationality();
        }, function($data, $status, $headers, $config) {

        })
        $scope.all_countries = Register.getCountry();
        Register.retrieveCountry($scope.device_type, function($data, $status, $headers, $config) {
            $scope.all_countries = Register.getCountry();
        }, function($data, $status, $headers, $config) {

        })

        $scope.keyboardSettings = {
            theme: 'light',
            showLetters: true,
            action: function(number) {
                // console.log(number);
                $scope.registerData.password_1 += number;


            },
            leftButton: {
                html: '',
                style: {
                    bgColor: '#d2d6db',
                }
            },
            rightButton: {
                html: '<i class="icon ion-backspace-outline"></i>',
                action: function() {
                    $scope.loginData.pwd = $scope.loginData.pwd.slice(0, -1);
                },
                style: {
                    bgColor: '#d2d6db',
                }
            }
        }
        $scope.keyboardSettings2 = {
            theme: 'light',
            showLetters: true,
            action: function(number) {

                $scope.registerData.password_2 += number;

            },
            leftButton: {
                html: '',
                style: {
                    bgColor: '#d2d6db',
                }
            },
            rightButton: {
                html: '<i class="icon ion-backspace-outline"></i>',
                action: function() {
                    $scope.loginData.pwd = $scope.loginData.pwd.slice(0, -1);
                },
                style: {
                    bgColor: '#d2d6db',
                }
            }
        }
        $scope.showKeyboardforPwd = function($event) {
            // cordova.plugins.Keyboard.close();
            $scope.keyboardVisible = true;
        }
        $scope.hideKeyboardforPwd = function($event) {
            $scope.keyboardVisible = false;
        }
        $scope.showKeyboardforPwd2 = function($event) {
            // cordova.plugins.Keyboard.close();
            $scope.keyboardVisible2 = true;
        }
        $scope.hideKeyboardforPwd2 = function($event) {
            $scope.keyboardVisible2 = false;
        }
        $scope.doRegister = function(form) {
            // console.log('ininin');
            // console.log($scope.registerData);
            // console.log($scope.registerData.agree);
            // $scope.hideKeyboardforPwd();
            // $scope.hideKeyboardforPwd2();


            if (!form.$invalid) {
                if (!$scope.registerData.agree) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Attention',
                        template: "You must agree to Terms & Conditions to Proceed.",
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                } else {
                    form.$submitted = false;
                    Register.registration($scope.device_type, $scope.registerData, function($data, $status, $headers, $config) {
                        $scope.registerData = [];
                    }, function($data, $status, $headers, $config) {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Error',
                            template: $data.msg,
                            buttons: [{
                                type: 'ion-btn',
                                text: 'OK'
                            }]
                        });
                    })
                }

            }
        }

    })
    .controller('ActivationCtrl', function($scope, $stateParams, $state, Register, $ionicPopup) {
        $scope.activateData = [];

        $scope.activateData.agree = false;
        $scope.keyboardSettings = {
            theme: 'light',
            showLetters: true,
            action: function(number) {
                // console.log(number);
                $scope.loginData.pwd += number;

            },
            leftButton: {
                html: '',
                style: {
                    bgColor: '#d2d6db',
                }
            },
            rightButton: {
                html: '<i class="icon ion-backspace-outline"></i>',
                action: function() {
                    $scope.loginData.pwd = $scope.loginData.pwd.slice(0, -1);
                },
                style: {
                    bgColor: '#d2d6db',
                }
            }
        }
        $scope.showKeyboard = function($event) {
            // cordova.plugins.Keyboard.close();
            $scope.keyboardVisible = true;
        }
        $scope.hideKeyboard = function($event) {
            $scope.keyboardVisible = false;
        }

        $scope.doActivation = function(form) {
            $scope.hideKeyboard();
            if (!form.$invalid) {
                if (!$scope.activateData.agree) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Attention',
                        template: "You must agree to Terms & Conditions to Proceed.",
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                } else {
                    form.$submitted = false;
                    Register.setActivation($scope.activateData);
                    Register.activate($scope.device_type, $scope.activateData, function($data, $status, $headers, $config) {
                        $scope.activateData = [];

                        $state.go('app.setpwd');
                    }, function($data, $status, $headers, $config) {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Error',
                            template: $data.msg,
                            buttons: [{
                                type: 'ion-btn',
                                text: 'OK'
                            }]
                        });
                    })
                }

            }
        }
    })
    .controller('SetPwdCtrl', function($scope, $stateParams, $state, $ionicPopup, Register) {
        $scope.setPwd_data = Register.getActivation();
        $scope.member_data = Register.getVirtualData();
        // console.log($scope.setPwd_data);
        $scope.doActivation = function(form) {
            // $scope.hideKeyboard();
            if (!form.$invalid) {
                Register.activate($scope.device_type, $scope.setPwd_data, $scope.member_data, function($data, $status, $headers, $config) {
                    $scope.setPwd_data = [];
                    $scope.member_data = [];
                }, function($data, $status, $headers, $config) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Error',
                        template: $data.msg,
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                })
            }
        }


    })
    .controller('ForgotPwdCtrl', function($scope, $stateParams, $state, $ionicPopup, ForgotPwd, Login) {
        $scope.forgotData = [];
        $scope.changePwdData = [];

        $scope.profile = Login.getProfile();
        $scope.resetPwd = function(form) {

            if (!form.$invalid) {
                form.$submitted = false;
                ForgotPwd.resetPwd($scope.device_type, $scope.forgotData, function($data, $status, $headers, $config) {
                    $scope.forgotData = [];
                }, function($data, $status, $headers, $config) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Error',
                        template: $data.msg,
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                })
            }
        }

        $scope.changePwd = function(form) {
            // console.log($scope.loginData);
            if (!form.$invalid) {
                form.$submitted = false;
                ForgotPwd.changePwd($scope.loginData.nric, $scope.device_type, $scope.profile.card_no, $scope.changePwdData, function($data, $status, $headers, $config) {
                    $scope.changePwdData = [];
                }, function($data, $status, $headers, $config) {
                    // var alertPopup = $ionicPopup.alert({
                    //      title: 'Error',
                    //      template: $data.msg,
                    //      buttons:[{
                    //         type: 'ion-btn',
                    //         text: 'OK'
                    //      }]
                    //   });
                })
            }
        }
    })
    .controller('TransactionCtrl', function($scope, $stateParams, $state, $ionicPopup, Transactions, Login, $ionicModal) {
        $scope.spending = [];
        $scope.receipt = [];
        $scope.redemption = [];



        $scope.last_updated_time = moment().format('DD MMM YYYY HH:mm');
        $scope.menu_select = 'spending';

        $scope.profile = Login.getProfile();

        $scope.spending = Transactions.getSpending();
        Transactions.allTransactions($scope.device_type, $scope.profile.card_no, function($data, $status, $headers, $config) {
            $scope.spending = Transactions.getSpending();

        }, function($data, $status, $headers, $config) {
            // var alertPopup = $ionicPopup.alert({
            //      title: 'Error',
            //      template: $data.msg,
            //      buttons:[{
            //         type: 'ion-btn',
            //         text: 'OK'
            //      }]
            //   });
        })


        $scope.openLegends = function() {
            $ionicModal.fromTemplateUrl('templates/legends.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.legends = modal;
                $scope.legends.show();
            });

        }
        $scope.closeLegends = function() {

            $scope.legends.hide();
        }

        $scope.changeMenu = function(selectedMenu) {

            $scope.menu_select = selectedMenu;
            if ($scope.menu_select == 'receipt') {
                $scope.receipt = Transactions.getReceipt();
                Transactions.allReceipts($scope.profile.card_no, function($data, $status, $headers, $config) {
                    $scope.receipt = Transactions.getReceipt();

                }, function($data, $status, $headers, $config) {
                    // var alertPopup = $ionicPopup.alert({
                    //      title: 'Error',
                    //      template: $data.msg,
                    //      buttons:[{
                    //         type: 'ion-btn',
                    //         text: 'OK'
                    //      }]
                    //   });
                })
            } else if ($scope.menu_select == 'spending') {
                $scope.spending = Transactions.getSpending();
                Transactions.allTransactions($scope.device_type, $scope.profile.card_no, function($data, $status, $headers, $config) {
                    $scope.spending = Transactions.getSpending();

                }, function($data, $status, $headers, $config) {
                    // var alertPopup = $ionicPopup.alert({
                    //      title: 'Error',
                    //      template: $data.msg,
                    //      buttons:[{
                    //         type: 'ion-btn',
                    //         text: 'OK'
                    //      }]
                    //   });
                })

            } else {

                $scope.redemption = Transactions.getRedemption();
                Transactions.allRedemptions($scope.device_type, $scope.profile.card_no, function($data, $status, $headers, $config) {
                    $scope.redemption = Transactions.getRedemption();

                }, function($data, $status, $headers, $config) {
                    // var alertPopup = $ionicPopup.alert({
                    //      title: 'Error',
                    //      template: $data.msg,
                    //      buttons:[{
                    //         type: 'ion-btn',
                    //         text: 'OK'
                    //      }]
                    //   });
                })

            }
        }
        $scope.doRefreshSpending = function() {
            $scope.$broadcast('scroll.refreshComplete');

            if ($scope.menu_select == 'spending') {
                Transactions.allTransactions($scope.device_type, $scope.profile.card_no, function($data, $status, $headers, $config) {
                    $scope.spending = Transactions.getSpending();

                }, function($data, $status, $headers, $config) {
                    // var alertPopup = $ionicPopup.alert({
                    //      title: 'Error',
                    //      template: $data.msg,
                    //      buttons:[{
                    //         type: 'ion-btn',
                    //         text: 'OK'
                    //      }]
                    //   });
                })
            } else if ($scope.menu_select == 'receipt') {
                $scope.last_updated_time = moment().format('DD MMM YYYY HH:mm');
                $scope.receipt = Transactions.getReceipt();;
                Transactions.allReceipts($scope.profile.member_id, function($data, $status, $headers, $config) {
                    $scope.receipt = Transactions.getReceipt();

                }, function($data, $status, $headers, $config) {
                    // var alertPopup = $ionicPopup.alert({
                    //      title: 'Error',
                    //      template: $data.msg,
                    //      buttons:[{
                    //         type: 'ion-btn',
                    //         text: 'OK'
                    //      }]
                    //   });
                })

            } else {
                $scope.redemption = Transactions.getRedemption();
                Transactions.allRedemptions($scope.device_type, $scope.profile.card_no, function($data, $status, $headers, $config) {
                    $scope.redemption = Transactions.getRedemption();

                }, function($data, $status, $headers, $config) {
                    // var alertPopup = $ionicPopup.alert({
                    //      title: 'Error',
                    //      template: $data.msg,
                    //      buttons:[{
                    //         type: 'ion-btn',
                    //         text: 'OK'
                    //      }]
                    //   });
                })
            }

        };


    })

.controller('ScanResultCtrl', function($scope, $stateParams, $state, $ionicPopup, $ionicModal, Transactions) {
    $scope.id = $stateParams.id;

    $scope.receipts = Transactions.getReceiptDetails($scope.id);
    $scope.receipt_img;

    $scope.switchReceipt = function(id) {
        // console.log(id);
        if (id < $scope.receipts.length) {
            $scope.id = id;
            $scope.shopname = $scope.receipts[id].shop;
            $scope.status = $scope.receipts[id].status;
            $scope.image = $scope.receipts[id].image;
            $scope.datetime = $scope.receipts[id].transaction_date;
        }
        // if(id < $scope.receipts.length){
        //   $scope.id = id;
        //   $scope.shopname = $scope.receipts[id].shop;
        //   $scope.status = $scope.receipts[id].status;
        //   $scope.img = $scope.receipts[id].img;
        //   $scope.datetime = $scope.receipts[id].datetime;
        // } 
    }
    $scope.switchReceipt(0);
    $scope.showReceipt = function(img) {
        // console.log(img);
        $scope.receipt_img = img;
        $ionicModal.fromTemplateUrl('templates/show_receipt.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.receipt = modal;
            // if(window.StatusBar) {
            //   StatusBar.style(1);
            // }
            $scope.receipt.show();
        });

    }
    $scope.closeReceipt = function() {
        $scope.receipt.hide();
    }
})

.controller('PrivilegeCtrl', function($scope, $stateParams, $state, $ionicPopup, Privileges, $ionicModal, $ionicScrollDelegate, Login) {
    $scope.privilege1 = [];
    $scope.gifts = [];

    $scope.profile = Login.getProfile();
    // console.log($scope.profile);
    $scope.privilege1 = Privileges.getPrivilege1();
    Privileges.allPrivileges($scope.device_type, $scope.profile.tier, function($data, $status, $headers, $config) {
        $scope.privilege1 = Privileges.getPrivilege1();
    }, function($data, $status, $headers, $config) {
        // var alertPopup = $ionicPopup.alert({
        //      title: 'Error',
        //      template: $data.msg,
        //      buttons:[{
        //         type: 'ion-btn',
        //         text: 'OK'
        //      }]
        //   });
    })


    $scope.menu_select = 'privilege1';
    $scope.filter_choice = "All";
    $scope.last_updated_time = moment().format('DD MMM YYYY HH:mm');
    $scope.openLegends = function() {
        $ionicModal.fromTemplateUrl('templates/legends.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.legends = modal;
            $scope.legends.show();
        });

    }
    $scope.closeLegends = function() {

        $scope.legends.hide();
    }

    $scope.changeMenu = function(selectedMenu) {

        $scope.menu_select = selectedMenu;
        if ($scope.menu_select == 'gift') {
            $scope.top = "gift-top";
            $scope.gifts = Privileges.getGifts();
            Privileges.allGifts($scope.device_type, $scope.profile.card_no, $scope.profile.member_id, function($data, $status, $headers, $config) {
                $scope.privilege1 = Privileges.getPrivilege1();
            }, function($data, $status, $headers, $config) {
                // var alertPopup = $ionicPopup.alert({
                //      title: 'Error',
                //      template: $data.msg,
                //      buttons:[{
                //         type: 'ion-btn',
                //         text: 'OK'
                //      }]
                //   });
            })
            $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
        } else {
            $scope.top = "";
        }
    }
    $scope.doRefreshPrivilege1 = function() {
        $scope.last_updated_time = moment().format('DD MMM YYYY HH:mm');
        if ($scope.menu_select == 'privilege1') {
            $scope.privilege1 = Privileges.getPrivilege1();
            Privileges.allPrivileges($scope.device_type, $scope.profile.tier, function($data, $status, $headers, $config) {
                $scope.privilege1 = Privileges.getPrivilege1();
                $scope.$broadcast('scroll.refreshComplete');
            }, function($data, $status, $headers, $config) {
                $scope.$broadcast('scroll.refreshComplete');
                // var alertPopup = $ionicPopup.alert({
                //      title: 'Error',
                //      template: $data.msg,
                //      buttons:[{
                //         type: 'ion-btn',
                //         text: 'OK'
                //      }]
                //   });
            })
        } else {
            $scope.gifts = Privileges.getGifts();
            Privileges.allGifts($scope.device_type, $scope.profile.card_no, $scope.profile.member_id, function($data, $status, $headers, $config) {
                $scope.privilege1 = Privileges.getPrivilege1();
                $scope.$broadcast('scroll.refreshComplete');
            }, function($data, $status, $headers, $config) {
                $scope.$broadcast('scroll.refreshComplete');
                // var alertPopup = $ionicPopup.alert({
                //      title: 'Error',
                //      template: $data.msg,
                //      buttons:[{
                //         type: 'ion-btn',
                //         text: 'OK'
                //      }]
                //   });
            })
        }

    };

    $scope.openFilter = function() {
        $ionicModal.fromTemplateUrl('templates/filter.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.filter = modal;
            $scope.filter.show();
        });
    }
    $scope.closeFilter = function() {
        $scope.filter.hide();
    }
    $scope.chooseChoice = function(choice) {
        // console.log(choice);
        $scope.filter_choice = choice;


    }

    $scope.openPSFilter = function() {
        $ionicModal.fromTemplateUrl('templates/privileges_search_filter.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.filter = modal;
            $scope.filter.show();
        });
    }
    $scope.closePSFilter = function() {
        $scope.filter.hide();
    }
    $scope.chooseChoicePS = function(choice) {
        // console.log(choice);
        $scope.filter_choice = choice;


    }

    $scope.openPrivilegesSearch = function() {
        $scope.redirect('privileges_search');
    }

})

.controller('PrivilegesSearchCtrl', function($scope, $stateParams, $state, $ionicPopup, Privileges, $ionicModal) {
    $scope.privilege1 = [];

    $scope.privilege1 = Privileges.getPrivilege1();
    // console.log($scope.privilege1);
    $scope.menu_select = 'privilege1';
    $scope.filter_choice = "All";

    $scope.openPSFilter = function() {
        $ionicModal.fromTemplateUrl('templates/privileges_search_filter.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.filter = modal;
            $scope.filter.show();
        });
    }
    $scope.closePSFilter = function() {
        $scope.filter.hide();
    }
    $scope.chooseChoicePS = function(choice) {
        // console.log(choice);
        $scope.filter_choice = choice;


    }

    $scope.search = {};
    $scope.search.ps_keyword = '';
    $scope.search_privileges = function() {
        // console.log($scope.search.ps_keyword);

        var tmp_privileges = Privileges.getPrivilege1();
        $scope.privilege1 = [];
        for (var i = 0; i < tmp_privileges.length; i++) {
            console.log(tmp_privileges[i].shop);
            if ($scope.search.ps_keyword == '') {
                $scope.privilege1.push(tmp_privileges[i]);
            } else if (tmp_privileges[i].shop.toLowerCase().includes($scope.search.ps_keyword.toLowerCase())) {
                $scope.privilege1.push(tmp_privileges[i]);
            }
        }
        console.log($scope.privilege1);
    }

})




.controller('DashboardCtrl', function($scope, $stateParams, $state, $ionicPopup, $ionicModal, Login, Dashboard, $ionicScrollDelegate, IUManager) {

        $scope.dashboard = {};
        $scope.dashboard.greeting_msg = 'Good Morning';


        // console.log($scope.dashboard);

        $scope.$on('$ionicView.enter', function() {
            // $scope.dashboard = Dashboard.getUserProfile();
            $scope.login_details = Login.getProfile();
            console.log($scope.login_details);
            if ($scope.login_details && $scope.login_details.nric && $scope.login_details.nric != "") {

                $scope.refreshUserProfile()
            } else {
                $state.go('app.login');
            }






        })
        $scope.getActiveIU = function() {
            $scope.dashboard.active_iu = IUManager.getIU();
            IUManager.getIUs($scope.dashboard.member_id, $scope.device_type, function($data, $status, $headers, $config) {
                $scope.dashboard.active_iu = IUManager.getIU();
                // console.log($scope.dashboard.active_iu);
                $scope.$broadcast('scroll.refreshComplete');


            }, function($data, $status, $headers, $config) {

            })

        }
        $scope.refreshUserProfile = function() {
            $scope.dashboard = Dashboard.getUserProfile();

            Dashboard.refreshUserProfile($scope.device_type, $scope.login_details.nric, function($data, $status, $headers, $config) {
                $scope.dashboard = Dashboard.getUserProfile();

                var d = new Date();

                if (d.getHours() >= 0 && d.getHours() < 12) {
                    $scope.dashboard.greeting_msg = 'Good Morning';
                } else if (d.getHours() >= 12 && d.getHours() < 18) {
                    $scope.dashboard.greeting_msg = 'Good Afternoon';
                } else {
                    $scope.dashboard.greeting_msg = 'Good Evening';
                }
                // console.log($scope.dashboard);
                // console.log($scope.dashboard.active_iu);
                $scope.getActiveIU();


            }, function($data, $status, $headers, $config) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Error',
                    template: $data.msg,
                    buttons: [{
                        type: 'ion-btn',
                        text: 'OK'
                    }]
                });
            })
        }

        // Dashboard.dashboard(function($data, $status, $headers, $config) {
        //         $scope.dashboard = {};
        //         $scope.dashboard = Dashboard.getData();
        //    }, function($data, $status, $headers, $config){
        //       var alertPopup = $ionicPopup.alert({
        //            title: 'Error',
        //            template: $data.msg,
        //            buttons:[{
        //               type: 'ion-btn',
        //               text: 'OK'
        //            }]
        //         });
        //    })




        $ionicModal.fromTemplateUrl('templates/dashboardmenu.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.d_menu = modal;
            // $scope.legends.show();
        });

        $scope.showMenu = function() {
            // console.log('showMenu');
            $scope.d_menu.show();
        }
        $scope.hideMenu = function() {
            $scope.d_menu.hide();
        }
        $scope.logout = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Logout',
                template: 'Do you want to logout?',
                okType: 'ion-btn'
            });

            confirmPopup.then(function(res) {
                if (res) {

                    $scope.hideMenu();
                    // cordova.plugins.IonGss.auth("");
                    $state.go('app.login');
                } else {
                    $scope.hideMenu();
                }
            });
        }
        $scope.doRefresh = function() {


            $scope.dashboard = Dashboard.getUserProfile();
            $scope.refreshUserProfile();
        }

        $scope.scrolltoQR = function() {
            $ionicScrollDelegate.$getByHandle('mainScroll').scrollBottom(true);
        }
    })
    .controller('IUManagerCtrl', function($scope, $stateParams, $state, $ionicPopup, IUManager, Login) {
        $scope.profile = Login.getProfile();

        $scope.checkIU = function() {

            $scope.iu_wrong = false;
            if ($scope.new_iu.confirm_iu_no != $scope.new_iu.iu_no) {

                $scope.iu_wrong = true;
            } else {

                $scope.iu_wrong = false;
            }
        }



        $scope.iu_menu_selected = 'set_delete_active_iu';

        $scope.changeIUMenu = function(menu_id) {
            $scope.iu_menu_selected = menu_id;
            if ($scope.profile.tier != "The 100" && $scope.profile.tier != "ION Privi Elite") {
                $scope.getActiveIU();
            } else {
                $scope.getMutipleActiveIU();
            }
        };

        $scope.iu = [];
        if ($scope.profile.tier != 'The 100' && $scope.profile.tier != "ION Privi Elite") {
            $scope.iu.active_iu = '';

        } else {
            $scope.iu.active_iu = [];
        }



        $scope.iu = IUManager.getIU();
        IUManager.getIUs($scope.profile.member_id, $scope.device_type, function($data, $status, $headers, $config) {
            $scope.iu = IUManager.getIU();

            if ($scope.profile.tier != 'The 100' && $scope.profile.tier != "ION Privi Elite") {
                $scope.getActiveIU();
            } else {
                $scope.getMutipleActiveIU();
            }

        }, function($data, $status, $headers, $config) {

        })

        $scope.getActiveIU = function() {
            $scope.iu.active_iu = IUManager.getActiveIU();
            // console.log($scope.iu.active_iu);
            $scope.iu.active_selected = $scope.iu.active_iu;
        }
        $scope.getMutipleActiveIU = function() {
            $scope.iu.active_iu = IUManager.getMultipleActiveIU();
        }



        $scope.set_selected_iu = function(iu_no) {

            $scope.iu.active_selected = iu_no;

        }
        $scope.set_mutiple_selected_iu = function(index) {
            console.log($scope.iu[index]['activate']);
            if ($scope.iu[index]['activate'] == false) {
                $scope.iu[index]['activate'] = true;
            } else {
                $scope.iu[index]['activate'] = false;
            }
            console.log($scope.iu[index]['activate']);
        }

        // $scope.iu.member_passsword = null;
        $scope.disable_done = false;
        $scope.checkPwd = function() {

            if ($scope.iu.member_passsword == null || $scope.iu.member_passsword.toString().length < 6) {
                $scope.disable_done = true;
            } else {
                $scope.disable_done = false;
            }
            // console.log($scope.disable_done);
        }

        $scope.set_iu_as_active = function() {
            // $scope.hideKeyboard();
            $ionicPopup.show({
                cssClass: 'iu-popup iu-popup-active',
                template: '<input type="number" class="pwd" pattern="[0-9]*" inputmode="numeric" ng-model="iu.member_passsword" limit-to="6" ng-change="checkPwd()" placeholder="Enter Passsword to Activate">',
                title: 'Activate IU',
                subTitle: $scope.iu.active_selected,
                scope: $scope,
                buttons: [{
                        text: 'I\'m Done',
                        type: 'button-positive',

                        onTap: function(e) {


                            if (!$scope.iu.member_passsword || $scope.iu.member_passsword.toString().length < 6) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                // console.log($scope.iu);
                                if ($scope.profile.tier != 'The 100' && $scope.profile.tier != "ION Privi Elite") {
                                    for (var i = 0; i < $scope.iu.length; i++) {
                                        if ($scope.iu[i].iu_no != $scope.iu.active_selected) {
                                            $scope.iu[i].activate = false
                                        } else {
                                            $scope.iu[i].activate = true
                                        }
                                    }
                                }

                                // call update active iu service
                                IUManager.activateIU($scope.device_type, $scope.profile.nric, $scope.profile.member_id, $scope.iu, $scope.iu.member_passsword, function($data, $status, $headers, $config) {
                                    $scope.iu.member_passsword = "";

                                    $scope.iu = IUManager.getIU();
                                    IUManager.getIUs($scope.profile.member_id, $scope.device_type, function($data, $status, $headers, $config) {
                                        $scope.iu = IUManager.getIU();
                                        if ($scope.profile.tier != 'The 100' && $scope.profile.tier != "ION Privi Elite") {
                                            $scope.getActiveIU();
                                        } else {
                                            $scope.getMutipleActiveIU();
                                        }

                                    }, function($data, $status, $headers, $config) {

                                    })

                                    if ($scope.profile.tier != 'The 100' && $scope.profile.tier != "ION Privi Elite") {
                                        $ionicPopup.show({
                                            title: 'Activated IU',
                                            template: 'IU ' + $scope.iu.active_selected + ' has been selected as your Active IU.',
                                            scope: $scope,
                                            buttons: [{
                                                text: 'OK',
                                                type: 'ion-btn'
                                            }]
                                        });
                                    } else {
                                        var iu_1 = '';
                                        var iu_2 = '';
                                        var iu_3 = '';
                                        if ($scope.iu.length != 0) {
                                            if ($scope.iu[0].activate == true) {
                                                iu_1 = 'IU ' + $scope.iu[0].iu_no + '<br/>';
                                            }
                                            if ($scope.iu[1]) {
                                                if ($scope.iu[1].activate == true) {
                                                    iu_2 = 'IU ' + $scope.iu[1].iu_no + '<br/>';
                                                }
                                            }
                                            if ($scope.iu[2]) {
                                                if ($scope.iu[2].activate == true) {
                                                    iu_3 = 'IU ' + $scope.iu[2].iu_no + '<br/>';

                                                }
                                            }

                                            $ionicPopup.show({
                                                title: 'Activated IU',
                                                template: iu_1 + iu_2 + iu_3 + ' have been selected as your Active IU.',
                                                scope: $scope,
                                                buttons: [{
                                                    text: 'OK',
                                                    type: 'ion-btn'
                                                }]
                                            });
                                        }

                                    }

                                }, function($data, $status, $headers, $config) {
                                    $scope.iu.member_passsword = "";
                                    $ionicPopup.show({
                                        title: 'Error',
                                        template: $data.msg,
                                        scope: $scope,
                                        buttons: [{
                                            text: 'OK',
                                            type: 'ion-btn'
                                        }]
                                    });

                                })


                                return $scope.iu.member_passsword;
                            }
                        }
                    },
                    { text: 'Cancel' }
                ]
            });
        };

        $scope.remove_iu = function(iu_no, index) {
            // $scope.hideKeyboard();


            $ionicPopup.show({
                cssClass: 'iu-popup iu-popup-delete',
                template: '<input type="password" ng-model="iu.member_passsword" placeholder="Enter Passsword to Delete" pattern="[0-9]*" inputmode="numeric">',
                title: 'Delete IU',
                subTitle: iu_no,
                scope: $scope,
                buttons: [{
                        text: 'I\'m Done',
                        type: 'button-positive',
                        onTap: function(e) {

                            if (!$scope.iu.member_passsword) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                // call delete iu service
                                $scope.iu.splice(index, 1);
                                IUManager.activateIU($scope.device_type, $scope.profile.nric, $scope.profile.member_id, $scope.iu, $scope.iu.member_passsword, function($data, $status, $headers, $config) {
                                    $scope.iu.member_passsword = "";

                                    $scope.iu = IUManager.getIU();
                                    IUManager.getIUs($scope.profile.member_id, $scope.device_type, function($data, $status, $headers, $config) {
                                        $scope.iu = IUManager.getIU();

                                        if ($scope.profile.tier != 'The 100' && $scope.profile.tier != 'ION Privi Elite') {
                                            $scope.getActiveIU();
                                        } else {
                                            $scope.getMutipleActiveIU();
                                        }

                                    }, function($data, $status, $headers, $config) {

                                    })
                                    $ionicPopup.show({
                                        title: 'Activated IU',
                                        template: 'IU ' + iu_no + ' has been Deleted.',
                                        scope: $scope,
                                        buttons: [{
                                            text: 'OK',
                                            type: 'ion-btn'
                                        }]
                                    });
                                }, function($data, $status, $headers, $config) {
                                    $scope.iu.member_passsword = "";
                                    $ionicPopup.show({
                                        title: 'Error',
                                        template: $data.msg,
                                        scope: $scope,
                                        buttons: [{
                                            text: 'OK',
                                            type: 'ion-btn'
                                        }]
                                    });

                                })

                                return $scope.iu.member_passsword;
                            }
                        }
                    },
                    { text: 'Cancel' }
                ]
            });
        };

        $scope.new_iu = [];
        // $scope.new_iu.iu_no = null;
        // $scope.new_iu.confirm_iu_no = null;
        // $scope.new_iu.confirm_password = null;
        // $scope.new_iu.is_active_iu = 0;

        $scope.new_iu_submit = function() {
            // $scope.hideKeyboard();
            if ($scope.iu.length < 3) {

                var str = $scope.new_iu.iu_no.toString();

                if (str.length < 10) {

                    $ionicPopup.show({
                        title: 'Error',
                        template: 'Invalid IU.',
                        scope: $scope,
                        buttons: [{
                            text: 'OK',
                            type: 'ion-btn'
                        }]
                    });
                } else {
                    $scope.iu.push({ 'iu_no': $scope.new_iu.iu_no, 'activate': $scope.new_iu.is_active_iu });
                    // console.log($scope.iu);
                    // call create iu service
                    IUManager.activateIU($scope.device_type, $scope.profile.nric, $scope.profile.member_id, $scope.iu, $scope.new_iu.confirm_password, function($data, $status, $headers, $config) {


                        $scope.iu = IUManager.getIU();
                        IUManager.getIUs($scope.profile.member_id, $scope.device_type, function($data, $status, $headers, $config) {
                            $scope.iu = IUManager.getIU();

                            if ($scope.profile.tier != "The 100" && $scope.profile.tier != "ION Privi Elite") {
                                $scope.getActiveIU();
                            } else {
                                $scope.getMutipleActiveIU();
                            }

                        }, function($data, $status, $headers, $config) {

                        })
                        var myPopup = $ionicPopup.show({
                            title: 'Added IU',
                            template: 'IU ' + $scope.new_iu.iu_no + ' has been added.',
                            scope: $scope,
                            buttons: [{
                                text: 'OK',
                                type: 'ion-btn'
                            }]
                        });

                        myPopup.then(function(res) {
                            $scope.new_iu = [];
                        });

                    }, function($data, $status, $headers, $config) {
                        $scope.iu.member_passsword = "";
                        $ionicPopup.show({
                            title: 'Error',
                            template: $data.msg,
                            scope: $scope,
                            buttons: [{
                                text: 'OK',
                                type: 'ion-btn'
                            }]
                        });

                    })
                }


            } else {
                $ionicPopup.show({
                    title: 'Error',
                    template: "You have reached the max. number of pre-registered IUs.",
                    scope: $scope,
                    buttons: [{
                        text: 'OK',
                        type: 'ion-btn'
                    }]
                });
            }


        }



    })
    .controller('EditProfileCtrl', function($scope, Register, EditProfile, IUManager, Login, $stateParams, $state, $ionicPopup) {

        $scope.editProfileData = Login.getProfile();
        // console.log($scope.editProfileData);
        $scope.editProfileData.contact = parseInt($scope.editProfileData.contact);
        // console.log($scope.editProfileData.dob);
        // $scope.editProfileData.dob = moment($scope.editProfileData.dob, "YYYY/MM/DD");
        $scope.editProfileData.dob = new Date($scope.editProfileData.dob);
        // console.log($scope.editProfileData.dob);
        if ($scope.editProfileData.unit == "null") {
            $scope.editProfileData.unit = "";
        }

        // $scope.editProfileData.nric = $scope.loginData.nric;
        // $scope.editProfileData.family_name = $scope.loginData.family_name;
        // $scope.editProfileData.given_name = $scope.loginData.given_name;
        // $scope.editProfileData.email = "shawnlee@gmail.com";
        // $scope.editProfileData.contact = "91234567";
        // $scope.editProfileData.gender = 'male';
        // $scope.editProfileData.gender = "male";
        // $scope.editProfileData.dob = new Date(99,5,24,11,33,30,0);
        // $scope.editProfileData.nationality = "Singapore";

        $scope.all_countries = Register.getCountry();
        Register.retrieveCountry($scope.device_type, function($data, $status, $headers, $config) {
            $scope.all_countries = Register.getCountry();

        }, function($data, $status, $headers, $config) {

        })

        $scope.all_nationalities = Register.getNationality();
        Register.retrieveNationality($scope.device_type, function($data, $status, $headers, $config) {
            $scope.all_nationalities = Register.getNationality();

        }, function($data, $status, $headers, $config) {

        })
        $scope.all_salutation = Register.getSalutation();
        Register.retrieveSalutation($scope.device_type, function($data, $status, $headers, $config) {
            $scope.all_salutation = Register.getSalutation();

        }, function($data, $status, $headers, $config) {

        })
        $scope.ius = IUManager.getIU();
        IUManager.getIUs($scope.editProfileData.member_id, $scope.device_type, function($data, $status, $headers, $config) {
            $scope.ius = IUManager.getIU();
            // console.log($scope.ius);

        }, function($data, $status, $headers, $config) {

        })

        $scope.submitProfile = function(form) {
            // console.log($scope.editProfileData);

            if (!form.$invalid) {
                form.$submitted = false;
                EditProfile.editProfile($scope.editProfileData, $scope.device_type, $scope.editProfileData.card_no, function($data, $status, $headers, $config) {
                    Login.setProfile($data.data);
                    $scope.editProfileData = $data.data;
                    // $scope.editProfileData.dob = moment($data.data.dob).format('DD/MM/YYYY')
                    $scope.editProfileData.dob = new Date($data.data.dob);
                    // console.log($scope.editProfileData.dob);
                    var alertPopup = $ionicPopup.alert({
                        title: 'Success',
                        template: "Profile updated successfully.",
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                }, function($data, $status, $headers, $config) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Error',
                        template: $data.msg,
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                })
            }
        }

    })
    .controller('TotalPointsCtrl', function($scope, Register, EditProfile, Login, $stateParams, $state, $ionicPopup) {
        $scope.doRefreshTotalPoints = function() {
            $scope.$broadcast('scroll.refreshComplete');
            $scope.last_updated_time = moment().format('DD MMM YYYY HH:mm');
        };

        $scope.points = Login.getProfile();
        $scope.points.ion_points_prev_expiry = new Date($scope.points.ion_points_prev_expiry);





    })

.controller('CarparkDollarCtrl', function($scope, CarparkDollar, Login, $stateParams, $state, $ionicPopup, IUManager) {
        $scope.transfer_points = [];
        $scope.transfer_points.iu_no = "";
        $scope.profile = Login.getProfile();
        // console.log($scope.profile.carpark_points[0].points);

        $scope.ius = IUManager.getIU();
        IUManager.getIUs($scope.profile.member_id, $scope.device_type, function($data, $status, $headers, $config) {
            $scope.ius = IUManager.getIU();
            // console.log($scope.ius);
            for (var i = 0; i < $scope.ius.length; i++) {
                if ($scope.ius[i].activate == true) {
                    $scope.transfer_points.iu_no = $scope.ius[i].iu_no;
                    break;
                } else {
                    $scope.transfer_points.iu_no = "";
                }
            }
        }, function($data, $status, $headers, $config) {

        })

        $scope.doTransfer = function(form) {

            if (!form.$invalid) {
                CarparkDollar.convertCarparkDollar($scope.device_type, $scope.profile.nric, $scope.profile.card_no, $scope.transfer_points, function($data, $status, $headers, $config) {
                    $scope.transfer_points = [];
                    form.$submitted = false;
                    var alertPopup = $ionicPopup.alert({
                        title: 'Success',
                        template: 'Points transferred successfully',
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                }, function($data, $status, $headers, $config) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Error',
                        template: $data.msg,
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                })
            }

        }


    })
    .controller('RewardsTncCtrl', function($scope, $stateParams, $state, $ionicPopup, TermsCondition) {
        $scope.tnc = [];

        // $scope.tnc = Login.getProfile();
        // console.log($scope.profile.carpark_points[0].points);

        $scope.tnc = TermsCondition.getTnc();
        TermsCondition.loadTerms(function($data, $status, $headers, $config) {
            $scope.tnc = TermsCondition.getTnc();


        }, function($data, $status, $headers, $config) {

        })


    })

.controller('ScanReceiptsCtrl', function($scope, ScanReceipt, $stateParams, $state, $ionicPopup, $cordovaCapture, $ionicPopover, $ionicLoading, $cordovaImagePicker, $ionicModal, $cordovaFileTransfer, $ionicScrollDelegate, $location) {
    $scope.receipts = [];
    $scope.currentCapture = [];
    $scope.stores = [];
    $scope.selectedStore = "";

    $scope.hide_footer = false;
    $scope.show_preview = false;

    $scope.scrollToAnchor = function(id) {
        // console.log(id);
        $location.hash(id);
        $ionicScrollDelegate.anchorScroll();
    };

    $scope.stores = ScanReceipt.getStoreList();;
    ScanReceipt.shoplist($scope.device_type, function($data, $status, $headers, $config) {

        $scope.stores = ScanReceipt.getStoreList();;
        // console.log($scope.stores);
        $scope.getFirstChar();
    }, function() {});

    $scope.selectStore = function(store) {
        $scope.selectedStore = store;
        $scope.closeStore();
        $scope.captureImage();
    }

    $scope.getFirstChar = function() {
        $scope.firstChars = [];
        $scope.names = [];

        // for (var i = 48; i <= 57; i++) {


        var tmp_arr = [];
        for (var j = 0; j < $scope.stores.length; j++) {
            // console.log($scope.stores[j].outlet_code);
            if ($scope.stores[j].outlet_code.charCodeAt(0) >= 48 && $scope.stores[j].outlet_code.charCodeAt(0) <= 57) {
                // console.log(String.fromCharCode(i));
                tmp_arr.push($scope.stores[j]);
            }
        }

        // console.log(String.fromCharCode(i));
        // console.log(tmp_arr);

        // if (tmp_arr.length == 0)  continue;

        // $scope.names = Object.assign($scope.names, JSON.parse('{"' + String.fromCharCode(i) + '": '+JSON.stringify(tmp_arr)+'}'));

        // console.log(String.fromCharCode(i));
        $scope.firstChars.push('#');
        $scope.names.push(tmp_arr);
        console.log($scope.names);
        // }
        for (var i = 65; i <= 90; i++) {


            var tmp_arr = [];
            for (var j = 0; j < $scope.stores.length; j++) {
                // console.log($scope.stores[j].outlet_code);
                if ($scope.stores[j].outlet_code.charAt(0) == String.fromCharCode(i)) {
                    // console.log(String.fromCharCode(i));
                    tmp_arr.push($scope.stores[j]);
                }
            }

            // console.log(String.fromCharCode(i));
            // console.log(tmp_arr);

            if (tmp_arr.length == 0) continue;

            // $scope.names = Object.assign($scope.names, JSON.parse('{"' + String.fromCharCode(i) + '": '+JSON.stringify(tmp_arr)+'}'));

            // console.log(String.fromCharCode(i));
            $scope.firstChars.push(String.fromCharCode(i));
            $scope.names.push(tmp_arr);
        }
    }

    $scope.captureImage = function() {

        $scope.openScanReceipt();
        $scope.startCamera();

    }

    $ionicPopover.fromTemplateUrl('templates/store.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(popover) {
        $scope.popover2 = popover;
    });

    $scope.openStore = function() {
        $scope.popover2.show();
    }
    $scope.closeStore = function() {
        $scope.popover2.hide();
    }


    $scope.upload = function() {
        var url = apiurl + 'upload_receipt.php';
        //File for Upload
        var targetPath = cordova.file.externalRootDirectory + $scope.currentCapture[0];

        var filename = targetPath.split("/").pop();
        // console.log("filename " + filename);
        var options = {
            fileKey: "receipt_1",
            fileName: filename,
            chunkedMode: false,
            mimeType: "image/jpg",
            params: {} // directory represents remote directory,  fileName represents final remote file name
        };

        $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {

            var receipt_response = JSON.parse(result.response);
            if ($scope.currentCapture.length > 1) {

                var options = {
                    fileKey: "receipt_1",
                    fileName: filename,
                    chunkedMode: false,
                    mimeType: "image/jpg",
                    params: { prev_url: receipt_response.data.receipt_image } // directory represents remote directory,  fileName represents final remote file name
                };
                $cordovaFileTransfer.upload(url, targetPath, options).then(function(result2) {
                    $scope.currentCapture = [];


                }, function(err) {
                    console.log("ERROR: " + JSON.stringify(err));
                }, function(progress) {
                    // PROGRESS HANDLING GOES HERE
                });
            } else {
                $scope.currentCapture = [];
            }

        }, function(err) {
            console.log("ERROR: " + JSON.stringify(err));
        }, function(progress) {
            // PROGRESS HANDLING GOES HERE
        });
    }

    // ========
    /**
     * Convert a base64 string in a Blob according to the data and contentType.
     * 
     * @param b64Data {String} Pure base64 string without contentType
     * @param contentType {String} the content type of the file i.e (image/jpeg - image/png - text/plain)
     * @param sliceSize {Int} SliceSize to process the byteCharacters
     * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
     * @return Blob
     */
    $scope.b64toBlob = function(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    /**
     * Create a Image file according to its database64 content only.
     * 
     * @param folderpath {String} The folder where the file will be created
     * @param filename {String} The name of the file that will be created
     * @param content {Base64 String} Important : The content can't contain the following string (data:image/png[or any other format];base64,). Only the base64 string is expected.
     */
    $scope.savebase64AsImageFile = function(folderpath, filename, content, contentType, success, failure) {
        // Convert the base64 string in a Blob
        var DataBlob = $scope.b64toBlob(content, contentType);

        console.log("Starting to write the file :3");
        var workerFolder = "";
        if (ionic.Platform.isIOS()) {
            workerFolder = cordova.file.tempDirectory;
        } else if (ionic.Platform.isAndroid()) {
            workerFolder = cordova.file.applicationStorageDirectory;
        }
        console.log(workerFolder);
        window.resolveLocalFileSystemURL(workerFolder, function(dir) {
            console.log("Access to the directory granted succesfully");
            dir.getFile(filename, { create: true }, function(file) {
                console.log("File created succesfully. " + file.name);
                file.createWriter(function(fileWriter) {
                    console.log("Writing content to file " + file.name);
                    fileWriter.write(DataBlob);
                    // alert(cordova.file.tempDirectory, +"/" + file.name);
                    success(file);

                }, function() {
                    // alert('Unable to save file in path ' + folderpath);
                });
            });
        });
    }

    $scope.startCamera = function() {
        CameraPreview.startCamera({ x: 0, y: 0, width: window.screen.width, height: window.screen.height, camera: "back", tapPhoto: true, previewDrag: false, toBack: true });
        CameraPreview.setPreviewSize({ width: window.screen.width, height: window.screen.height });
    }

    $scope.takeFromGallery = function() {
        var options = {
            maximumImagesCount: 1,
            width: 1024,
            height: 1024,
            quality: 100
        };

        $cordovaImagePicker.getPictures(options)
            .then(function(results) {
                for (var i = 0; i < results.length; i++) {
                    // console.log('Image URI: ' + results[i]);
                    $scope.currentCapture.push(results[i]);
                }
                $scope.closeScanReceiptPopover();
            }, function(error) {
                // error getting photos
                $scope.closeScanReceiptPopover();
            });
    }

    $scope.takePicture = function(success, failure) {
        CameraPreview.takePicture(function(imgData) {
            $ionicLoading.show();
            $scope.currentFileName = "ION-" + $scope.currentCapture.length + ".jpg";
            $scope.savebase64AsImageFile("ION Orchard Receipts", $scope.currentFileName, imgData, "image/jpeg", function(file) {
                var workerFolder = "";
                if (ionic.Platform.isIOS()) {
                    workerFolder = cordova.file.tempDirectory;
                } else if (ionic.Platform.isAndroid()) {
                    workerFolder = cordova.file.applicationStorageDirectory;
                }
                console.log(workerFolder);
                window.resolveLocalFileSystemURL(workerFolder, function(dir) {
                    dir.getFile($scope.currentFileName, { create: false }, function(file) {
                        $ionicLoading.hide();
                        $scope.currentCapture.push(workerFolder + "/" + $scope.currentFileName);
                        if ($scope.currentCapture.length >= 2) {


                            $scope.closeScanReceiptPopover();
                        }
                    });
                }, function() {
                    $ionicLoading.hide();
                    // alert("No receipt file!");
                });
            });
        });
    }

    $ionicPopover.fromTemplateUrl('templates/take_receipt.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover = popover;
    });

    $scope.openScanReceipt = function() {
        $scope.currentCapture = [];
        $scope.hideNavView();
        $scope.popover.show();
        $scope.hide_footer = false;
        $scope.show_preview = false;
    };

    $scope.closeScanReceipt = function() {
        $scope.showNavView();
        CameraPreview.hide();
        CameraPreview.stopCamera();
        $scope.popover.hide();
    }

    $scope.closeScanReceiptPopover = function() {
        if ($scope.currentCapture.length > 0) {
            $scope.hide_footer = true;
            $scope.show_preview = true;
        } else {
            $scope.closeScanReceipt();
        }
    };

    $scope.clearCloseScanReceiptPopover = function() {
        $scope.currentCapture = [];
        $scope.closeScanReceipt();
    };


    $scope.noUseScanReceipt = function() {
        $scope.hide_footer = false;
        $scope.show_preview = false;
        $scope.currentCapture = [];
    }

    $scope.useScanReceipt = function() {
        $scope.hide_footer = false;
        $scope.show_preview = false;

        // $scope.upload();
        $scope.currentCapture = [];
        // $scope.receipts.push($scope.currentCapture);
        $scope.closeScanReceiptPopover();
    }

    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.popover.remove();
        $scope.modal.remove();
    });
})