angular.module('starter.services', [])

.factory('Login', function($http, $ionicLoading) {
    var Login = [{
        profile:[]
    }]

    Login.dologin = function(data, devicetype, successcb, failurecb, $ionicPopup){
       
        $ionicLoading.show({
            template: '<ion-spinner></ion-spinner>'+
            '<p>Signing...</p>',
            // template: "Signing In",
            // template:"<i class='ion-load-b'><i/> Signing In",
            duration: 2000
        });

        
        $http({
                method: 'POST',
                url: apiurl + 'login.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: devicetype, nric: data.nric, password: data.pwd}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {
                       
                        sessionStorage.setItem('userid', $data.data.user_id);
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }
    Login.setProfile = function(data){
        Login.profile = data;
        // Login.profile.dob = moment(data.dob, 'yyyy/MM/DD');
    }

    Login.getProfile = function(){
        // console.log(Login.profile);
        return Login.profile;
    }

    return Login;
})
.factory('Register', function($http, $ionicLoading, $ionicPopup) {
    var Register = [{
        activateData : [],
        activateVirtual:[]
    }]

     Register.registration = function(device_type, data, successcb, failurecb){
        $ionicLoading.show();
        // console.log(data);
        var dob = moment(data.dob).format('YYYY-MM-DD');

        if(data.IU_no == undefined){
            data.IU_no = '';
        }
        if(data.building == undefined){
            data.building = '';
        }
        if(data.level == undefined){
            data.level = '';
        }
        if(data.unit == undefined){
            data.unit = '';
        }
        $http({
                method: 'POST',
                url: apiurl + 'register.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: device_type, sign_up_id: data.sign_up_id, nric: data.nric, family_name: data.family_name, given_name: data.given_name, email: data.email, contact: data.contact, dob: dob, salutation: 'Mr', gender: data.gender, nationality: data.nationality, iu_no: data.IU_no, password: data.password_1, postal_code: data.postal_code, street: data.street, building: data.building, block: data.block, level: data.level, unit: data.unit, country: data.country, country_code: data.country_code, area_code: '', notify_sms: data.pdpa.sms, notify_post: data.pdpa.post, notify_email: data.pdpa.email, notify_call: data.pdpa.call, iu_no: data.IU_no}
            }). 
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {
                       
                         var alertPopup = $ionicPopup.alert({
                             title: 'Account Activated',
                             template: 'You can now proceed to login.',
                             okType: 'ion-ok-btn'
                         });

                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }

    Register.retrieveSalutation = function(device_type, successcb, failurecb){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'get_system_code.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: device_type, parent_code:'Salutation'}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {
                       
                        Register.salutation = $data.data;
                        // console.log(Register.salutation);
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }
    Register.retrieveNationality = function(device_type, successcb, failurecb){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'get_system_code.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: device_type, parent_code:'Nationality'}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {
                       
                        Register.nationality = $data.data;
                        // console.log(Register.nationality);
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }
    Register.retrieveCountry = function(device_type, successcb, failurecb){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'get_system_code.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: device_type, parent_code:'Country'}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {
                       
                        Register.country = $data.data;
                        // console.log(Register.country);
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }
    Register.activate = function(device_type, data, successcb, failurecb){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'check_member_is_activated.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: device_type, nric: data.nric, email: data.email, contact_no: data.contact}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {
                        Register.activateVirtual = $data.data;
                         // var alertPopup = $ionicPopup.alert({
                         //     title: 'Account Activated',
                         //     template: 'You can now proceed to login.'
                         // });

                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }
    Register.setPwd = function(device_type, setPwd_data, member_data, data, successcb, failurecb){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'activate_membership.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: device_type, nric: setPwd_data.nric, family_name: member_data.email, given_name: member_data.given_name, gender:member_data.gender, password: setPwd_data.password_1}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {
                        
                         var alertPopup = $ionicPopup.alert({
                             title: 'Account Activated',
                             template: 'You can now proceed to login.'
                         });

                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }
    Register.getVirtualData = function(){
        return Register.activateVirtual;
    }
    Register.getCountry = function(){
        return Register.country;
    }
    Register.getSalutation = function(){
        return Register.salutation;
    }
    Register.getNationality = function(){
        return Register.nationality;
    }
    Register.setActivation = function(data){
        Register.activateData = data;
        console.log(Register.activateData);
    }
    Register.getActivation = function(){
        return Register.activateData;
    }
    return Register;
})
.factory('ForgotPwd', function($http, $ionicLoading, $ionicPopup) {
    var Password = []

     Password.resetPwd = function(device_type, data, successcb, failurecb){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'forgot_password.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: device_type, nric: data.nric, email: data.email}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {
                       
                         var alertPopup = $ionicPopup.alert({
                             title: 'Password Reset',
                             template: 'Password Reset Successfully.',
                             okType: 'ion-ok-btn'
                         });

                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }
    Password.changePwd = function(nric, devicetype, card_no, data, successcb, failurecb){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'change_password.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: devicetype, nric: nric, password: data.new_pwd, card_no: card_no, current_password: data.current_pwd}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {
                       
                         var alertPopup = $ionicPopup.alert({
                             title: 'Success',
                             template: 'Password Changed Successfully.',
                             okType: 'ion-ok-btn'
                         });

                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        var alertPopup = $ionicPopup.alert({
                             title: 'Error',
                             template: $data.msg,
                             okType: 'ion-ok-btn'
                         });
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "");
                }
            });
    }

    
    return Password;
})
.factory('Transactions', function($http, $ionicLoading, $ionicPopup) {
    var Transactions = {
        spending: [],
        receipts:[],
        redemptions:[]
    }

    Transactions.allTransactions = function(device_type, card_no, successcb, failurecb, $ionicPopup){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'get_spending_history.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: device_type, card_no: card_no}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {

                        Transactions.spending = $data.data;
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }

    Transactions.allReceipts = function(card_no, successcb, failurecb, $ionicPopup){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'get_receipt_history.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {card_no: card_no}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {

                        Transactions.receipts = $data.data;
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }

    Transactions.allRedemptions = function(device_id, card_no, successcb, failurecb, $ionicPopup){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'get_redemption_history.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: device_id, card_no: card_no}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {

                        Transactions.redemptions = $data.data;
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }
    Transactions.getPrivilege1 = function(){
        for(var i = 0; i < Transactions.transactions.length; i++){
            if(Transactions.transactions[i].type == 'privileges'){

                return Transactions.transactions[i].details;
            }
        }
    }

    Transactions.getGift = function(){
        for(var i = 0; i < Transactions.transactions.length; i++){
            if(Transactions.transactions[i].type == 'gift'){

                return Transactions.transactions[i].details;
            }
        }
    }
    Transactions.getReceipt = function(){

        return Transactions.receipts;
        
    }
    Transactions.getReceiptDetails = function(id){
        //take note id is the $index of array
        // console.log(Transactions.receipts[id]['receipts']);
        return Transactions.receipts[id]['receipts'];
    }
    Transactions.getRedemption = function(){
        return Transactions.redemptions;
    }

    Transactions.getSpending = function(){
      
            return Transactions.spending;
      }
    return Transactions;
})

.factory('Privileges', function($http, $ionicLoading, $ionicPopup) {
    var Privileges = {
        privileges: [],
        gifts:[],
        dining:[],
        shopping:[],
    }

    Privileges.allPrivileges = function(device_type, tier, successcb, failurecb, $ionicPopup){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'get_member_privileges.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: device_type, tier: tier}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {

                        Privileges.privileges = $data.data;
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }

    Privileges.allGifts = function(device_type, card_no, member_id, successcb, failurecb, $ionicPopup){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'gifts_availability.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: device_type, card_no: card_no, member_id: member_id}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {

                        // Privileges.privileges = $data.data;
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }

    Privileges.getPrivilege1 = function(){
        
        return Privileges.privileges;
         
    }

    Privileges.getReceipt = function(){
        for(var i = 0; i < Privileges.privileges.length; i++){
            if(Privileges.privileges[i].type == 'receipt'){

                return Privileges.privileges[i].details;
            }
        }
    }
    Privileges.getGifts = function(){
        for(var i = 0; i < Privileges.privileges.length; i++){
            if(Privileges.privileges[i].type == 'gift'){

                return Privileges.privileges[i].details;
            }
        }
    }
   

    return Privileges;
})


.factory('EditProfile', function($http, $ionicLoading, $ionicPopup) {
    var Profile = []

    Profile.editProfile = function(data, devicetype, card_no, successcb, failurecb, $ionicPopup){
        $ionicLoading.show();
        // console.log(data.pdpa);
        // if(data.pdpa.sms == undefined || data.pdpa.sms == "undefined"){
        //         data.pdpa.sms = false;
        // }  
        // if(data.pdpa.post == undefined || data.pdpa.post == "undefined"){
        //      data.pdpa.post = false;
        // }
        var dob = moment(data.dob).format('YYYY-MM-DD');
        if(data.building == undefined){
            data.building = '';
        }

        $http({
                method: 'POST',
                url: apiurl + 'edit_profile.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: devicetype, nric: data.nric, family_name: data.family_name, given_name: data.given_name, email: data.email, contact: data.contact, dob: dob, salutation: data.salutation, gender: data.gender, nationality: data.nationality, postal_code: data.postal_code, street: data.street, building: data.building, block: data.block, level: data.level, unit: data.unit, country:data.country, country_code: data.country_code, area_code: data.area_code, notify_sms: data.notify_sms, notify_post: data.notify_post, notify_call: data.notify_call, notify_email: data.notify_email, card_no: card_no}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {

                        
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }

    

    return Profile;
})
.factory('Dashboard', function($http, $ionicLoading, $ionicPopup) {
    var Dashboard = {
        data:{
            name:"Shawn Lee",
            nric:"S91xxxxxZ",
            membership_status:"Active till 2016-12-11",
            ion_plus_point: 0,
            iu_no: "1122334455",
            qr_img: "img/qrcode.png",
            banner: "img/ion_plus_rewards.png"
        }
    };

    Dashboard.dashboard = function(successcb, failurecb, $ionicPopup){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'dashboard',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {

                        Dashboard.data = $data;
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }
    Dashboard.getData = function(){
        
        return Dashboard.data;
    }

    return Dashboard;
})
.factory('IUManager', function($http, $ionicLoading, $ionicPopup) {
    var IUManager = [{
        iu: []
    }];
    IUManager.getIUs = function(member_id, device_type, successcb, failurecb, $ionicPopup){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'get_iu_list.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {member_id: member_id, device_type: device_type}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {

                        IUManager.iu = $data.data;
                        // console.log(IUManager.iu);
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }
    IUManager.activateIU = function(device_type, nric, member_id, iu_list, password, successcb, failurecb, $ionicPopup){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'activate_iu.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {member_id: member_id, device_type: device_type, nric: nric, password: password, iu_list:JSON.stringify(iu_list)}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {

                        
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }
    IUManager.getIU = function(){
        
        return IUManager.iu;
    }
   
    IUManager.getActiveIU = function(){
        // console.log(IUManager.iu.length);
        var iu = "";
        for(var i = 0; i < IUManager.iu.length; i++){
            // console.log(IUManager.iu[i].activate == true);
            if(IUManager.iu[i].activate == true){
                
                iu = IUManager.iu[i].iu_no;
                break;
              
            } else {
                iu = "";
            }
        }

        return iu;
    }
    IUManager.getMultipleActiveIU = function(){
        var iu = [];
        for(var i = 0; i < IUManager.iu.length; i++){
            if(IUManager.iu[i].activate == true){

                iu.push({iu_no: IUManager.iu[i].iu_no});
            } 
        }
        // console.log(iu);
        return iu;
    }

    return IUManager;
})
.factory('TermsCondition', function($http, $ionicLoading, $ionicPopup) {
    var terms = [];

     terms.loadTerms = function(successcb, failurecb, $ionicPopup){
        $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'http://illigo.sg/client/ion/revamp2016/en/?option=com_ionapp&task=get_ion_rewards_tnc&format=raw',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {
                        terms = $data.data.tnc;
                        
                        
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }

    terms.getTnc = function(){
        return terms;
    }

    return terms;
})
.factory('CarparkDollar', function($http, $ionicLoading, $ionicPopup) {
    var CarparkDollar = [];

    CarparkDollar.convertCarparkDollar = function(devicetype, nric, card_no, data, successcb, failurecb, $ionicPopup){
         $ionicLoading.show();
         
        $http({
                method: 'POST',
                url: apiurl + 'convert_carpark_dollar.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: devicetype, nric: nric, card_no: card_no, point_to_be_redeemed: data.points, password: data.password}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {
                        
                        
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });

    }

    return CarparkDollar;
})
.factory('ScanReceipt', function($http, $ionicLoading, $ionicPopup) {
    var ScanReceipt = [{
        shops:[]
    }];

    ScanReceipt.shoplist = function(device_type, successcb, failurecb, $ionicPopup){
         $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'get_store_list.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: device_type}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {
                        
                        ScanReceipt.shops = $data.data;
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });
    }
    ScanReceipt.getStoreList = function(){
        return ScanReceipt.shops;
    }
    return ScanReceipt;
})
.factory('Dashboard', function($http, $ionicLoading, $ionicPopup) {
    var Dashboard = [];

    Dashboard.refreshUserProfile = function(device_type, nric, successcb, failurecb, $ionicPopup){
         $ionicLoading.show();
        $http({
                method: 'POST',
                url: apiurl + 'get_user_profile.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {device_type: device_type, nric: nric}
            }).
            success(function($data, $status, $headers, $config) {
              
               $ionicLoading.hide();
               
                // console.log(Customers.list_of_customer);
                if ($data) {
                    if ($data.code == 200) {
                        
                        Dashboard.userprofile = $data.data;
                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                $ionicLoading.hide();
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Sign Up.");
                }
            });

    }
    Dashboard.getUserProfile = function(){
        return Dashboard.userprofile;
    }
    return Dashboard;
})
