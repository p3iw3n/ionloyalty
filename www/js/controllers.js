angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $state, $ionicHistory, $ionicPlatform) {
    $scope.apiurl = apiurl;

    $scope.$on('$ionicView.enter', function() {
        $ionicPlatform.registerBackButtonAction(function(event) {
            if ($state.current.name == "app.login" || $state.current.name == "app.dashboard") {
                cordova.plugins.IonGss.hitRoot();
            }
            event.preventDefault();
        }, 500);
    });

    $scope.isIOS = ionic.Platform.isIOS();
    $scope.campaignID;
    $scope.dealID;
    // console.log(isIOS);
    $scope.device_type;
    if ($scope.isIOS == true) {
        $scope.device_type = 'ios'
    } else {
        $scope.device_type = 'android'
    }

    // Form data for the login modal
    $scope.loginData = {};

    $scope.backtoION = function() {
        cordova.plugins.IonGss.openWhatsOn();
        cordova.plugins.IonGss.tabBar("1");
    }

    // Triggered in the login modal to close it
    $scope.closeRewards = function() {
        // console.log("Which Idiot!");
        if ($scope.modal) {
            $scope.modal.hide();
            $scope.modal = null;
        }
    };

    // Open the login modal
    $scope.rewards = function() {
        // console.log('OPEN YOU FOOL!!!!!');
        // Create the login modal that we will use later
        // console.log($scope.modal);
        // if (!$scope.modal) {
        $ionicModal.fromTemplateUrl('templates/rewards.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            if ($scope.modal) {
                console.log("hahahah");
                $scope.modal.hide();
            }
            $scope.modal = modal;
            console.log("hahahah 2");
            $scope.modal.show();
            if (typeof(cordova) !== 'undefined') {
                cordova.plugins.IonGss.tabBar("0");
            }

        });
        // } else {
        //     console.log("------ HEY STUPID ------");
        // }
    };

    $scope.showRegisterLanding = function() {
        // console.log(1);
        $ionicModal.fromTemplateUrl('templates/register_landing.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            // console.log(2);
            $scope.register_landing = modal;
            $scope.register_landing.show();
            // $scope.showRegisterLanding();
        });
        // console.log(3);

    };

    $scope.closeRegisterLanding = function() {
        if ($scope.register_landing) {
            $scope.register_landing.hide();
        }
    };
    $scope.gototnc = function() {

        $state.go('app.ionrewards_tnc');
    }

    $scope.redirect = function(action) {
        if (action == 'backtoroot') {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('app.dashboard');
        } else if (action == 'login') {
            console.log('loginnnnnn');
            $scope.closeRewards();
            $state.go('app.login');
        } else if (action == 'register') {
            $scope.closeRewards();
            // $scope.showRegisterLanding();
            // $state.go('app.register');
            $scope.showRegisterLanding();

        } else if (action == 'backLogin') {
            $scope.closeRegisterLanding();
            $scope.rewards();
        } else if (action == 'newMember') {
            $scope.closeRegisterLanding();
            $state.go('app.register');
        } else if (action == 'Dashboard') {
            // $scope.closeRegisterLanding();
            $state.go('app.dashboard');
        } else if (action == 'Privileges') {
            $state.go('app.privileges');
        } else if (action == 'Transactions') {
            $state.go('app.transactions');
        } else if (action == 'Privilege') {
            $state.go('app.privilege');
        } else if (action == 'IUManager') {
            $state.go('app.iu_manager');
        } else if (action == 'ScanReceipts') {
            // $ionicHistory.nextViewOptions({
            //     disableBack: true
            // });
            $state.go('app.scan_receipts');
        } else if (action == 'activate') {
            $scope.closeRegisterLanding();
            $state.go('app.activate');
        } else if (action == 'privileges_search') {
            $state.go('app.privileges_search');
        } else if (action == 'total_points') {
            $state.go('app.total_points');
        } else if (action == 'Carpark') {
            $state.go('app.carpark_dollar');
        } else if (action == 'IONSkyBook') {
            // $ionicHistory.nextViewOptions({
            //     disableBack: true
            // });
            $state.go('app.ionskybook');
        } else if (action == 'IONSkyDetail') {
            $state.go('app.ionskyticketdetail');
        } else if (action == 'IONSkyWallet') {
            // $ionicHistory.nextViewOptions({
            //     disableBack: true
            // });
            $state.go('app.ionskywallet');
        }
    }
    $scope.backView = function() {

        if ($state.current.name != "app.dashboard" && typeof($state.previous) === 'undefined') {
            $scope.redirect('backtoroot');
        } else {
            $ionicHistory.goBack();
        }
        // $ionicHistory.backView();
    }

    $scope.hideNavView = function() {
        document.getElementById('nav-view').classList.add('hide_nav_view');

        html = document.getElementsByTagName('html');
        if (html.length > 0) {
            html[0].classList.add("hide_nav_view_transparent");
        }
        body = document.getElementsByTagName('body');
        if (body.length > 0) {
            body[0].classList.add("hide_nav_view_transparent");
            body[0].classList.add("show_guide_line");
        }
        ionapp = document.getElementsByTagName('ion-app');
        if (ionapp.length > 0) {
            ionapp[0].classList.add("hide_nav_view_transparent");
        }
        ioncontent = document.getElementsByTagName('ion-content');
        if (ioncontent.length > 0) {
            ioncontent[0].classList.add("hide_nav_view_transparent");
        }
    }

    $scope.showNavView = function() {
        document.getElementById('nav-view').classList.remove('hide_nav_view');

        ioncontent = document.getElementsByTagName('ion-content');
        if (ioncontent.length > 0) {
            ioncontent[0].classList.remove("hide_nav_view_transparent");
        }
        ionapp = document.getElementsByTagName('ion-app');
        if (ionapp.length > 0) {
            ionapp[0].classList.remove("hide_nav_view_transparent");
        }
        body = document.getElementsByTagName('body');
        if (body.length > 0) {
            body[0].classList.remove("show_guide_line");
            body[0].classList.remove("hide_nav_view_transparent");
        }
        html = document.getElementsByTagName('html');
        if (html.length > 0) {
            html[0].classList.remove("hide_nav_view_transparent");
        }
    }

    $scope.openUrl = function(url) {
        cordova.plugins.IonGss.openUrl(url);
    }
})


.controller('LoginCtrl', function($scope, $stateParams, $state, Login, $ionicPopup, $window, $timeout) {
    // $scope.loginData = [];
    // $scope.loginData.pwd = '';
    // console.log('loginCtrl');
    $scope.$on('$ionicView.enter', function() {
        $scope.rewards();
        $scope.loginData = {};
        console.log($scope.loginData);
        $scope.campaignID = $stateParams.campaignID;
        $scope.dealID = $stateParams.dealID;
        console.log($scope.dealID);
        console.log("Session ID: " + localStorage.getItem('session_id'));
        if (localStorage.getItem('session_id')) {
            console.log("got session id");
            $scope.sessionLogin(localStorage.getItem('session_id'));
        }
    });

    $scope.loginData = {};

    $scope.keyboardVisible = false;
    $scope.keyboardSettings = {
        theme: 'light',
        showLetters: true,
        action: function(number) {
            // console.log(number);
            $scope.loginData.pwd += number;

        },
        leftButton: {
            html: '',
            style: {
                bgColor: '#d2d6db',
            }
        },
        rightButton: {
            html: '<i class="icon ion-backspace-outline"></i>',
            action: function() {
                $scope.loginData.pwd = $scope.loginData.pwd.slice(0, -1);
            },
            style: {
                bgColor: '#d2d6db',
            }
        }
    }
    $scope.showKeyboard = function($event) {
        // cordova.plugins.Keyboard.close();
        $scope.keyboardVisible = true;
    }
    $scope.hideKeyboard = function($event) {
        $scope.keyboardVisible = false;
    }

    $scope.doLogin = function(form) {
        // console.log($scope.loginData);
        // $scope.hideKeyboard();
        if (!form.$invalid) {
            $scope.login();
            form.$submitted = false;
        }

    }
    String.prototype.isEmpty = function() {
        if (!this.match(/\S/)) {
            return true;
        } else {
            return false;
        }
    }
    $scope.sessionLogin = function(session_id) {
        Login.loginSessionID(session_id, $scope.device_type, function($data, $status, $headers, $config) {
            form.$submitted = false;

            sessionStorage.setItem('username', $scope.loginData.nric);
            // console.log(JSON.stringify($scope.loginData));
            // $scope.closeRewards();

            for (var keys in $scope.loginData) {
                if (keys === "$submitted") {
                    delete $scope.loginData[keys];
                }
            }

            //console.log(JSON.stringify($scope.loginData));

            $window.localStorage.setItem('logindetail', JSON.stringify($scope.loginData));

            var currentTier = 1;
            switch ($data.data.tier) {
                case "ION Rewards":
                    currentTier = 1;
                    break;

                case "ION Privi":
                    currentTier = 2;
                    break;

                case "The 100":
                    currentTier = 3;
                    break;

                case "ION Privi Elite":
                    currentTier = 4;
                    break;

                default:
                    break;
            }

            // JC Check Here
            if (typeof(cordova) !== 'undefined') {
                // console.log(currentTier);
                cordova.plugins.IonGss.auth($scope.loginData.nric, currentTier, $data.data.fullname, $data.data.email, $data.data.contact);
                cordova.plugins.IonGss.tabBar("1");
            }
            $scope.loginData = {};
            $scope.loginData.card_no = $data.data.card_no;
            // console.log($scope.loginData.card_no);
            // console.log("-------- Start Login Data -----------")
            // console.log($data.data);
            Login.setProfile($data.data);
            // console.log(Login.getProfile());

            // $scope.loginData.nric = $data.data.nric;
            // $scope.loginData.card_no = $data.data.card_no;
            // $scope.loginData.area_code = $data.data.area_code;
            // $scope.loginData.country_code = $data.data.country_code;
            // $scope.loginData.contact = $data.data.contact;
            // $scope.loginData.family_name = $data.data.family_name;
            // $scope.loginData.given_name = $data.data.given_name;
            // $scope.loginData.dob = $data.data.dob;
            // $scope.loginData.gender = $data.data.gender;
            // $scope.loginData.nationality = $data.data.nationality;
            // $scope.loginData.block = $data.data.block;
            // $scope.loginData.street = $data.data.street;
            // $scope.loginData.level = $data.data.level;
            // $scope.loginData.postal_code = $data.data.postal_code;
            // $scope.loginData.building = $data.data.building;
            // $scope.loginData.country = $data.data.country;

            sessionStorage.setItem('firstLogin', false);

            $scope.closeRewards();
            if (typeof(cordova) !== 'undefined' && $scope.campaignID.isEmpty() == false && $scope.dealID.isEmpty() == false) {
                cordova.plugins.IonGss.openCampaign($scope.campaignID, $scope.dealID, $data.data.fullname, $data.data.email, $data.data.contact, $data.data.nric);
            }

            var lastLoginView = localStorage.getItem("last_login_view");
            console.log(lastLoginView);
            if (lastLoginView != null && lastLoginView != "") {
                localStorage.removeItem("last_login_view");
                $scope.redirect(lastLoginView);
            } else {
                $scope.redirect('Dashboard');
            }

        }, function($data, $status, $headers, $config, $errormsg) {

            var title = "Login";

            var alertPopup = $ionicPopup.alert({
                title: "",
                template: ($data) ? $data.msg : "Login Failed",
                buttons: [{
                    type: 'ion-btn',
                    text: 'OK'
                }]
            });
            alertPopup.then(function(res) {
                if ($data.code == 104) {
                    $window.localStorage.removeItem('logindetail');
                    Login.setProfile($data.data);
                    sessionStorage.setItem('firstLogin', true);
                    $state.go('app.changePwd');
                }
            });
        })
    }

    $scope.login = function() {
        Login.dologin($scope.loginData, $scope.device_type, function($data, $status, $headers, $config) {
            form.$submitted = false;
            console.log("Login");
            console.log("-------- Start Campaign ID-----------")
            console.log("Campaign ID: " + $scope.campaignID);
            console.log($scope.campaignID.isEmpty() == false);
            console.log("-------- End Campaign ID-----------")
            console.log("-------- Start Deal ID-----------")
            console.log("Deal ID: " + $scope.dealID);
            console.log($scope.dealID.isEmpty() == false);
            console.log("-------- End Deal ID-----------")

            sessionStorage.setItem('username', $scope.loginData.nric);
            console.log(JSON.stringify($scope.loginData));

            for (var keys in $scope.loginData) {
                if (keys === "$submitted") {
                    delete $scope.loginData[keys];
                }
            }

            console.log(JSON.stringify($scope.loginData));

            $window.localStorage.setItem('logindetail', JSON.stringify($scope.loginData));

            var currentTier = 1;
            switch ($data.data.tier) {
                case "ION Rewards":
                    currentTier = 1;
                    break;

                case "ION Privi":
                    currentTier = 2;
                    break;

                case "The 100":
                    currentTier = 3;
                    break;

                case "ION Privi Elite":
                    currentTier = 4;
                    break;

                default:
                    break;
            }
            if (typeof(cordova) !== 'undefined' && $scope.campaignID.isEmpty() == false && $scope.dealID.isEmpty() == false) {
                cordova.plugins.IonGss.openCampaign($scope.campaignID, $scope.dealID, $data.data.fullname, $data.data.email, $data.data.contact, $data.data.nric);
            }

            // JC Check Here
            if (typeof(cordova) !== 'undefined') {
                // console.log(currentTier);
                cordova.plugins.IonGss.auth($scope.loginData.nric, currentTier, $data.data.fullname, $data.data.email, $data.data.contact);
                cordova.plugins.IonGss.tabBar("1");
            }
            $scope.loginData = {};
            $scope.loginData.card_no = $data.data.card_no;
            // console.log($scope.loginData.card_no);
            // console.log($data.data);
            Login.setProfile($data.data);
            // console.log(Login.getProfile());

            // $scope.loginData.nric = $data.data.nric;
            // $scope.loginData.card_no = $data.data.card_no;
            // $scope.loginData.area_code = $data.data.area_code;
            // $scope.loginData.country_code = $data.data.country_code;
            // $scope.loginData.contact = $data.data.contact;
            // $scope.loginData.family_name = $data.data.family_name;
            // $scope.loginData.given_name = $data.data.given_name;
            // $scope.loginData.dob = $data.data.dob;
            // $scope.loginData.gender = $data.data.gender;
            // $scope.loginData.nationality = $data.data.nationality;
            // $scope.loginData.block = $data.data.block;
            // $scope.loginData.street = $data.data.street;
            // $scope.loginData.level = $data.data.level;
            // $scope.loginData.postal_code = $data.data.postal_code;
            // $scope.loginData.building = $data.data.building;
            // $scope.loginData.country = $data.data.country;

            sessionStorage.setItem('firstLogin', false);

            if (typeof(cordova) !== 'undefined' && $scope.campaignID.isEmpty() == false && $scope.dealID.isEmpty() == false) {
                cordova.plugins.IonGss.openCampaign($scope.campaignID, $scope.dealID, $data.data.fullname, $data.data.email, $data.data.contact, $data.data.nric);
            }

            var lastLoginView = localStorage.getItem("last_login_view");
            console.log(lastLoginView);
            if (lastLoginView != null && lastLoginView != "") {
                localStorage.removeItem("last_login_view");
                $scope.redirect(lastLoginView);
            } else {
                $scope.redirect('Dashboard');
            }
        }, function($data, $status, $headers, $config, $errormsg) {

            var title = "Login";

            var alertPopup = $ionicPopup.alert({
                title: "",
                template: ($data) ? $data.msg : "Login Failed",
                buttons: [{
                    type: 'ion-btn',
                    text: 'OK'
                }]
            });
            alertPopup.then(function(res) {
                if ($data.code == 104) {
                    $window.localStorage.removeItem('logindetail');
                    Login.setProfile($data.data);
                    sessionStorage.setItem('firstLogin', true);
                    $state.go('app.changePwd');
                }
            });
        })
    }

    // Get Login from storage.
    form = JSON.parse($window.localStorage.getItem('logindetail') || '{}');

    // $window.localStorage.setItem('logindetail') = JSON.stringify(value);

    // console.log(form);
    // console.log(JSON.stringify(form) != '{}');
    // if (JSON.stringify(form) != '{}') {

    // $timeout(function() {
    //     // $scope.redirect('login');
    //     // $scope.loginData = form;
    //     // $scope.login();
    //     // console.log('aaaa');
    //     // console.log(localStorage.getItem('session_id'));
    //     if(localStorage.getItem('session_id')){
    //         $scope.sessionLogin(localStorage.getItem('session_id'));
    //     }

    // }, 1000);
    // }
})


.controller('RegisterCtrl', function($scope, $stateParams, $state, Register, $ionicPopup) {

        // $scope.keyboardVisible = false;
        $scope.registerData = [];
        $scope.registerData.input_iu = true;
        $scope.registerData.gender = '';
        $scope.registerData.salutation = '';
        $scope.registerData.country_code = '';
        $scope.registerData.nationality = '';
        $scope.registerData.country = '';
        $scope.registerData.sign_up_id = '';
        $scope.registerData.agree = false;
        $scope.registerData.pdpa = [];
        $scope.registerData.pdpa['sms'] = false;
        $scope.registerData.pdpa['post'] = false;
        $scope.registerData.pdpa['email'] = false;
        $scope.registerData.pdpa['call'] = false;

        $scope.validNRIC = true;

        $scope.validateNRIC = function(str) {

            if (str.length != 9) {

                $scope.validNRIC = false;
                return false;
            } else {

                str = str.toUpperCase();

                var i,
                    icArray = [];
                for (i = 0; i < 9; i++) {
                    icArray[i] = str.charAt(i);
                }

                icArray[1] = parseInt(icArray[1], 10) * 2;
                icArray[2] = parseInt(icArray[2], 10) * 7;
                icArray[3] = parseInt(icArray[3], 10) * 6;
                icArray[4] = parseInt(icArray[4], 10) * 5;
                icArray[5] = parseInt(icArray[5], 10) * 4;
                icArray[6] = parseInt(icArray[6], 10) * 3;
                icArray[7] = parseInt(icArray[7], 10) * 2;

                var weight = 0;
                for (i = 1; i < 8; i++) {
                    weight += icArray[i];
                }

                var offset = (icArray[0] == "T" || icArray[0] == "G") ? 4 : 0;
                var temp = (offset + weight) % 11;

                var st = ["J", "Z", "I", "H", "G", "F", "E", "D", "C", "B", "A"];
                var fg = ["X", "W", "U", "T", "R", "Q", "P", "N", "M", "L", "K"];

                var theAlpha;
                if (icArray[0] == "S" || icArray[0] == "T") { theAlpha = st[temp]; } else if (icArray[0] == "F" || icArray[0] == "G") { theAlpha = fg[temp]; }

                $scope.validNRIC = (icArray[8] === theAlpha);
                console.log($scope.validNRIC);
                // console.log(icArray[8] === theAlpha);
                return (icArray[8] === theAlpha);

            }




        }

        $scope.salutation = Register.getSalutation();
        // Register.retrieveSalutation($scope.device_type, function($data, $status, $headers, $config) {
        //     $scope.salutation = Register.getSalutation();
        // }, function($data, $status, $headers, $config) {

        // })
        $scope.nationality = Register.getNationality();
        // Register.retrieveNationality($scope.device_type, function($data, $status, $headers, $config) {
        //     $scope.nationality = Register.getNationality();
        // }, function($data, $status, $headers, $config) {

        // })
        $scope.all_countries = Register.getCountry();
        // Register.retrieveCountry($scope.device_type, function($data, $status, $headers, $config) {
        //     $scope.all_countries = Register.getCountry();
        // }, function($data, $status, $headers, $config) {

        // })

        // $scope.keyboardSettings = {
        //     theme: 'light',
        //     showLetters: true,
        //     action: function(number) {
        //         // console.log(number);
        //         $scope.registerData.password_1 += number;


        //     },
        //     leftButton: {
        //         html: '',
        //         style: {
        //             bgColor: '#d2d6db',
        //         }
        //     },
        //     rightButton: {
        //         html: '<i class="icon ion-backspace-outline"></i>',
        //         action: function() {
        //             $scope.loginData.pwd = $scope.loginData.pwd.slice(0, -1);
        //         },
        //         style: {
        //             bgColor: '#d2d6db',
        //         }
        //     }
        // }
        // $scope.keyboardSettings2 = {
        //     theme: 'light',
        //     showLetters: true,
        //     action: function(number) {

        //         $scope.registerData.password_2 += number;

        //     },
        //     leftButton: {
        //         html: '',
        //         style: {
        //             bgColor: '#d2d6db',
        //         }
        //     },
        //     rightButton: {
        //         html: '<i class="icon ion-backspace-outline"></i>',
        //         action: function() {
        //             $scope.loginData.pwd = $scope.loginData.pwd.slice(0, -1);
        //         },
        //         style: {
        //             bgColor: '#d2d6db',
        //         }
        //     }
        // }
        // $scope.showKeyboardforPwd = function($event) {
        //     // cordova.plugins.Keyboard.close();
        //     $scope.keyboardVisible = true;
        // }
        // $scope.hideKeyboardforPwd = function($event) {
        //     $scope.keyboardVisible = false;
        // }
        // $scope.showKeyboardforPwd2 = function($event) {
        //     // cordova.plugins.Keyboard.close();
        //     $scope.keyboardVisible2 = true;
        // }
        // $scope.hideKeyboardforPwd2 = function($event) {
        //     $scope.keyboardVisible2 = false;
        // }
        $scope.doRegister = function(form) {
            // console.log('ininin');
            // console.log($scope.registerData);
            // console.log($scope.registerData.agree);
            // $scope.hideKeyboardforPwd();
            // $scope.hideKeyboardforPwd2();
            console.log(form);

            if (!form.$invalid) {
                if (!$scope.registerData.agree) {
                    var alertPopup = $ionicPopup.alert({
                        title: '',
                        template: "You must agree to Terms & Conditions to Proceed.",
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                } else {
                    if ($scope.registerData.sign_up_id == 'NRIC' || $scope.registerData.sign_up_id == 'FIN') {
                        $scope.validateNRIC($scope.registerData.nric)
                    } else {
                        $scope.validNRIC = true;
                    }

                    if ($scope.validNRIC) {
                        Register.registration($scope.device_type, $scope.registerData, function($data, $status, $headers, $config) {
                            $scope.registerData = [];
                            form.$submitted = false;
                            var alertPopup = $ionicPopup.alert({
                                title: '',
                                template: 'Please proceed to login.',
                                okType: 'ion-ok-btn'
                            });
                            alertPopup.then(function() {
                                $state.go('app.login');
                            });

                        }, function($data, $status, $headers, $config) {
                            form.$submitted = false;
                            var alertPopup = $ionicPopup.alert({
                                title: '',
                                template: $data.msg,
                                buttons: [{
                                    type: 'ion-btn',
                                    text: 'OK'
                                }]
                            });
                        })
                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: '',
                            template: "Invalid NRIC/FIN.",
                            buttons: [{
                                type: 'ion-btn',
                                text: 'OK'
                            }]
                        });
                    }

                }

            }
        }

    })
    .controller('ActivationCtrl', function($scope, $stateParams, $state, Register, $ionicPopup) {
        sessionStorage.setItem('activate_virtual_dob_is_set', '0');
        $scope.activateData = [];

        $scope.title = "<span class='two-line-title'>Activate Virtual Membership<br/>Account</span>"

        $scope.activateData.agree = false;
        $scope.keyboardSettings = {
            theme: 'light',
            showLetters: true,
            action: function(number) {
                // console.log(number);
                $scope.loginData.pwd += number;

            },
            leftButton: {
                html: '',
                style: {
                    bgColor: '#d2d6db',
                }
            },
            rightButton: {
                html: '<i class="icon ion-backspace-outline"></i>',
                action: function() {
                    $scope.loginData.pwd = $scope.loginData.pwd.slice(0, -1);
                },
                style: {
                    bgColor: '#d2d6db',
                }
            }
        }
        $scope.showKeyboard = function($event) {
            // cordova.plugins.Keyboard.close();
            $scope.keyboardVisible = true;
        }
        $scope.hideKeyboard = function($event) {
            $scope.keyboardVisible = false;
        }

        $scope.doActivation = function(form) {
            $scope.hideKeyboard();
            if (!form.$invalid) {
                if (!$scope.activateData.agree) {
                    var alertPopup = $ionicPopup.alert({
                        title: '',
                        template: "You must agree to Terms & Conditions to Proceed.",
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                } else {
                    form.$submitted = false;
                    Register.setActivation($scope.activateData);
                    Register.activate($scope.device_type, $scope.activateData, function($data, $status, $headers, $config) {
                        $scope.activateData = [];

                        $state.go('app.setpwd');
                    }, function($data, $status, $headers, $config) {
                        var alertPopup = $ionicPopup.alert({
                            title: '',
                            template: $data.msg,
                            buttons: [{
                                type: 'ion-btn',
                                text: 'OK'
                            }]
                        });
                    })
                }

            }
        }
    })
    .controller('SetPwdCtrl', function($scope, $stateParams, $state, $ionicPopup, $ionicPlatform, Register) {

        $scope.title = "<span class='two-line-title'>Activate Virtual Membership<br/>Account</span>";
        $scope.setPwd_data = Register.getActivation();

        $scope.member_data = Register.getVirtualData();

        $scope.dob_disabled = false;
        if (sessionStorage.getItem('activate_virtual_dob_is_set') == '1') {
            $scope.dob_disabled = true;
        }
        console.log($scope.dob_disabled);


        console.log(JSON.stringify($scope.member_data));
        if ($scope.member_data.dob != "" && $scope.member_data.dob != null) {
            $scope.member_data.dob = new Date($scope.member_data.dob);
        }


        //$ionicPlatform.registerBackButtonAction(function(event) {

        // event.preventDefault();
        //}, 100);
        // console.log($scope.setPwd_data);
        $scope.doActivation = function(form) {
            // $scope.hideKeyboard();
            if (!form.$invalid) {
                Register.setPwd($scope.device_type, $scope.setPwd_data, $scope.member_data, function($data, $status, $headers, $config) {
                    $scope.setPwd_data = [];
                    $scope.member_data = [];
                    form.$submitted = false;
                    var alertPopup = $ionicPopup.alert({
                        title: '',
                        template: 'Please proceed to login.',
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                    alertPopup.then(function() {
                        $state.go('app.login');
                    });

                }, function($data, $status, $headers, $config) {
                    var alertPopup = $ionicPopup.alert({
                        title: '',
                        template: $data.msg,
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                });
                // Register.setPwd($scope.device_type, $scope.setPwd_data, $scope.member_data, function($data, $status, $headers, $config) {
                //     // $scope.setPwd_data = [];
                //     // $scope.member_data = [];
                //     console.log('aaaa');
                //     $state.go('app.login');
                // }, function($data, $status, $headers, $config) {
                //     // var alertPopup = $ionicPopup.alert({
                //     //     title: 'Error',
                //     //     template: $data.msg,
                //     //     buttons: [{
                //     //         type: 'ion-btn',
                //     //         text: 'OK'
                //     //     }]
                //     // });
                // })
            }
        }


    })
    .controller('ForgotPwdCtrl', function($scope, $stateParams, $state, $ionicPopup, ForgotPwd, Login, $window) {
        $scope.forgotData = [];
        $scope.changePwdData = [];
        // console.log('99999999');

        console.log("isFirstLogin: " + sessionStorage.getItem('firstLogin'));
        $scope.isFirstLogin = (sessionStorage.getItem('firstLogin') == "true") ? "yes" : "no";
        // console.log("isFirstLogin: " + $scope.isFirstLogin);

        $scope.$on('$ionicView.enter', function() {
            $scope.profile = Login.getProfile();
            // console.log($scope.profile);
            // console.log($scope.loginData);
        });

        $scope.profile = Login.getProfile();
        // console.log($scope.profile);
        // console.log($scope.loginData);


        $scope.logout = function() {

            $window.localStorage.setItem('logindetail', "");
            localStorage.removeItem('session_id');

            $scope.loginData = {};
            $scope.profile = {};
            Login.setProfile([]);
            if (typeof(cordova) !== 'undefined') {
                cordova.plugins.IonGss.auth("");
                cordova.plugins.IonGss.tabBar("0");
            }
        }



        $scope.resetPwd = function(form) {

            if (!form.$invalid) {
                form.$submitted = false;
                ForgotPwd.resetPwd($scope.device_type, $scope.forgotData, function($data, $status, $headers, $config) {
                    $scope.forgotData = [];
                }, function($data, $status, $headers, $config) {
                    var alertPopup = $ionicPopup.alert({
                        title: '',
                        template: $data.msg,
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                })
            }
        }

        $scope.changePwd = function(form) {
            console.log($scope.loginData);
            console.log($scope.profile);

            if (!form.$invalid) {
                form.$submitted = false;

                // var $nric = ($scope.profile && $scope.profile.nric) ? $scope.profile.nric : $scope.loginData.nric;
                // var $nric = ($scope.loginData && $scope.loginData.nric && $scope.isFirstLogin == "yes") ? $scope.loginData.nric : $scope.profile.nric;
                var $nric = $scope.profile.nric;
                ForgotPwd.changePwd($nric, $scope.device_type, $scope.profile.card_no, $scope.changePwdData,
                    ($scope.isFirstLogin == "yes" ? true : false),
                    function($data, $status, $headers, $config) {
                        $scope.changePwdData = [];
                        // console.log(sessionStorage.getItem('firstLogin'));
                        // console.log($scope.isFirstLogin);
                        if ($scope.isFirstLogin == "yes") {
                            var alertPopup = $ionicPopup.alert({
                                title: '',
                                template: 'Password has been updated.',
                                okType: 'ion-ok-btn'
                            });


                        } else {
                            var alertPopup = $ionicPopup.alert({
                                title: '',
                                template: 'Password has been changed. Please proceed to login.',
                                okType: 'ion-ok-btn'
                            });

                            $window.localStorage.removeItem('logindetail');
                            $scope.loginData = {};
                            $scope.profile = {};
                            $state.go('app.login');
                        }

                        if (sessionStorage.getItem('firstLogin') == 'true') {

                            $scope.logout();
                            sessionStorage.removeItem('firstLogin');
                            $state.go('app.login');
                        }
                    },
                    function($data, $status, $headers, $config) {
                        // var alertPopup = $ionicPopup.alert({
                        //      title: 'Error',
                        //      template: $data.msg,
                        //      buttons:[{
                        //         type: 'ion-btn',
                        //         text: 'OK'
                        //      }]
                        //   });
                    })
            }
        }
    })
    .controller('TransactionCtrl', function($scope, $stateParams, $state, $ionicPopup, Transactions, Login, $ionicModal) {
        $scope.spending = [];
        $scope.receipt = [];
        $scope.redemption = [];



        $scope.last_updated_time = moment().format('DD MMM YYYY HH:mm');
        $scope.menu_select = 'spending';

        $scope.profile = Login.getProfile();

        $scope.spending = Transactions.getSpending();
        Transactions.allTransactions($scope.device_type, $scope.profile.card_no, function($data, $status, $headers, $config) {
            $scope.spending = Transactions.getSpending();

        }, function($data, $status, $headers, $config) {
            // var alertPopup = $ionicPopup.alert({
            //      title: 'Error',
            //      template: $data.msg,
            //      buttons:[{
            //         type: 'ion-btn',
            //         text: 'OK'
            //      }]
            //   });
        })


        $scope.openLegends = function() {
            $ionicModal.fromTemplateUrl('templates/legends.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.legends = modal;
                $scope.legends.show();
            });

        }
        $scope.closeLegends = function() {

            $scope.legends.hide();
        }

        $scope.changeMenu = function(selectedMenu) {

            $scope.menu_select = selectedMenu;
            if ($scope.menu_select == 'receipt') {
                $scope.receipt = Transactions.getReceipt();
                Transactions.allReceipts($scope.profile.card_no, function($data, $status, $headers, $config) {
                    $scope.receipt = Transactions.getReceipt();


                }, function($data, $status, $headers, $config) {
                    // var alertPopup = $ionicPopup.alert({
                    //      title: 'Error',
                    //      template: $data.msg,
                    //      buttons:[{
                    //         type: 'ion-btn',
                    //         text: 'OK'
                    //      }]
                    //   });
                })
            } else if ($scope.menu_select == 'spending') {
                $scope.spending = Transactions.getSpending();
                Transactions.allTransactions($scope.device_type, $scope.profile.card_no, function($data, $status, $headers, $config) {
                    $scope.spending = Transactions.getSpending();

                }, function($data, $status, $headers, $config) {
                    // var alertPopup = $ionicPopup.alert({
                    //      title: 'Error',
                    //      template: $data.msg,
                    //      buttons:[{
                    //         type: 'ion-btn',
                    //         text: 'OK'
                    //      }]
                    //   });
                })

            } else {

                $scope.redemption = Transactions.getRedemption();
                Transactions.allRedemptions($scope.device_type, $scope.profile.card_no, function($data, $status, $headers, $config) {
                    $scope.redemption = Transactions.getRedemption();


                }, function($data, $status, $headers, $config) {
                    // var alertPopup = $ionicPopup.alert({
                    //      title: 'Error',
                    //      template: $data.msg,
                    //      buttons:[{
                    //         type: 'ion-btn',
                    //         text: 'OK'
                    //      }]
                    //   });
                })

            }
        }
        $scope.doRefreshSpending = function() {
            $scope.$broadcast('scroll.refreshComplete');

            if ($scope.menu_select == 'spending') {
                Transactions.allTransactions($scope.device_type, $scope.profile.card_no, function($data, $status, $headers, $config) {
                    $scope.spending = Transactions.getSpending();

                }, function($data, $status, $headers, $config) {
                    // var alertPopup = $ionicPopup.alert({
                    //      title: 'Error',
                    //      template: $data.msg,
                    //      buttons:[{
                    //         type: 'ion-btn',
                    //         text: 'OK'
                    //      }]
                    //   });
                })
            } else if ($scope.menu_select == 'receipt') {
                $scope.last_updated_time = moment().format('DD MMM YYYY HH:mm');
                $scope.receipt = Transactions.getReceipt();;
                Transactions.allReceipts($scope.profile.member_id, function($data, $status, $headers, $config) {
                    $scope.receipt = Transactions.getReceipt();

                }, function($data, $status, $headers, $config) {
                    // var alertPopup = $ionicPopup.alert({
                    //      title: 'Error',
                    //      template: $data.msg,
                    //      buttons:[{
                    //         type: 'ion-btn',
                    //         text: 'OK'
                    //      }]
                    //   });
                })

            } else {
                $scope.redemption = Transactions.getRedemption();
                Transactions.allRedemptions($scope.device_type, $scope.profile.card_no, function($data, $status, $headers, $config) {
                    $scope.redemption = Transactions.getRedemption();

                }, function($data, $status, $headers, $config) {
                    // var alertPopup = $ionicPopup.alert({
                    //      title: 'Error',
                    //      template: $data.msg,
                    //      buttons:[{
                    //         type: 'ion-btn',
                    //         text: 'OK'
                    //      }]
                    //   });
                })
            }

        };


    })

.controller('ScanResultCtrl', function($scope, $stateParams, $state, $ionicPopup, $ionicModal, Transactions) {
    $scope.id = $stateParams.id;

    $scope.receipt = Transactions.getReceipt();
    console.log($scope.receipt);
    console.log($scope.receipt[0]['details_viewable']);

    $scope.receipts = Transactions.getReceiptDetails($scope.id);
    $scope.receipt_img;
    $scope.viewable = true;

    $scope.switchReceipt = function(id) {
        // console.log(id);
        console.log($scope.receipts);
        if (id < $scope.receipts.length) {
            $scope.id = id;
            $scope.shopname = $scope.receipts[id].shop;
            $scope.status = $scope.receipts[id].status;
            $scope.image = $scope.receipts[id].image;
            $scope.datetime = $scope.receipts[id].transaction_date;
            $scope.viewable = $scope.receipt[$stateParams.id].details_viewable;
            $scope.rejected_reason = $scope.receipts[id].rejected_reason;
            // console.log($scope.viewable);
        }
        // if(id < $scope.receipts.length){
        //   $scope.id = id;
        //   $scope.shopname = $scope.receipts[id].shop;
        //   $scope.status = $scope.receipts[id].status;
        //   $scope.img = $scope.receipts[id].img;
        //   $scope.datetime = $scope.receipts[id].datetime;
        // } 
    }
    $scope.switchReceipt(0);
    $scope.showReceipt = function(img) {
        console.log(img);
        $scope.receipt_img = img;

        $ionicModal.fromTemplateUrl('templates/show_receipt.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.receipt = modal;
            // if(window.StatusBar) {
            //   StatusBar.style(1);
            // }
            console.log('aaa');
            console.log($scope.viewable);
            if ($scope.viewable) {

                $scope.receipt.show();
            }

        });

    }
    $scope.closeReceipt = function() {
        $scope.receipt.hide();
    }
})

.controller('PrivilegeCtrl', function($scope, $stateParams, $state, $ionicPopup, Privileges, $ionicModal, $ionicScrollDelegate, Login) {
    $scope.privilege1 = [];
    $scope.gifts = [];

    $scope.profile = Login.getProfile();
    // console.log($scope.profile);
    $scope.privilege1 = Privileges.getPrivilege1();
    Privileges.allPrivileges($scope.device_type, $scope.profile.tier, function($data, $status, $headers, $config) {
        $scope.privilege1 = Privileges.getPrivilege1();
    }, function($data, $status, $headers, $config) {
        // var alertPopup = $ionicPopup.alert({
        //      title: 'Error',
        //      template: $data.msg,
        //      buttons:[{
        //         type: 'ion-btn',
        //         text: 'OK'
        //      }]
        //   });
    })


    $scope.menu_select = 'privilege1';
    $scope.filter_choice = "All";
    $scope.last_updated_time = moment().format('DD MMM YYYY HH:mm');
    $scope.openLegends = function() {
        $ionicModal.fromTemplateUrl('templates/legends.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.legends = modal;
            $scope.legends.show();
        });

    }
    $scope.closeLegends = function() {

        $scope.legends.hide();
    }

    $scope.changeMenu = function(selectedMenu) {

        $scope.menu_select = selectedMenu;
        if ($scope.menu_select == 'gift') {
            $scope.top = "gift-top";
            $scope.gifts = Privileges.getGifts();
            Privileges.allGifts($scope.device_type, $scope.profile.card_no, $scope.profile.member_id, function($data, $status, $headers, $config) {
                $scope.privilege1 = Privileges.getPrivilege1();
            }, function($data, $status, $headers, $config) {
                // var alertPopup = $ionicPopup.alert({
                //      title: 'Error',
                //      template: $data.msg,
                //      buttons:[{
                //         type: 'ion-btn',
                //         text: 'OK'
                //      }]
                //   });
            })
            $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
        } else {
            $scope.top = "";
        }
    }
    $scope.doRefreshPrivilege1 = function() {
        $scope.last_updated_time = moment().format('DD MMM YYYY HH:mm');
        if ($scope.menu_select == 'privilege1') {
            $scope.privilege1 = Privileges.getPrivilege1();
            Privileges.allPrivileges($scope.device_type, $scope.profile.tier, function($data, $status, $headers, $config) {
                $scope.privilege1 = Privileges.getPrivilege1();
                $scope.$broadcast('scroll.refreshComplete');
            }, function($data, $status, $headers, $config) {
                $scope.$broadcast('scroll.refreshComplete');
                // var alertPopup = $ionicPopup.alert({
                //      title: 'Error',
                //      template: $data.msg,
                //      buttons:[{
                //         type: 'ion-btn',
                //         text: 'OK'
                //      }]
                //   });
            })
        } else {
            $scope.gifts = Privileges.getGifts();
            Privileges.allGifts($scope.device_type, $scope.profile.card_no, $scope.profile.member_id, function($data, $status, $headers, $config) {
                $scope.privilege1 = Privileges.getPrivilege1();
                $scope.$broadcast('scroll.refreshComplete');
            }, function($data, $status, $headers, $config) {
                $scope.$broadcast('scroll.refreshComplete');
                // var alertPopup = $ionicPopup.alert({
                //      title: 'Error',
                //      template: $data.msg,
                //      buttons:[{
                //         type: 'ion-btn',
                //         text: 'OK'
                //      }]
                //   });
            })
        }

    };

    $scope.openFilter = function() {
        $ionicModal.fromTemplateUrl('templates/filter.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.filter = modal;
            $scope.filter.show();
        });
    }
    $scope.closeFilter = function() {
        $scope.filter.hide();
    }
    $scope.chooseChoice = function(choice) {
        // console.log(choice);
        $scope.filter_choice = choice;


    }

    $scope.openPSFilter = function() {
        $ionicModal.fromTemplateUrl('templates/privileges_search_filter.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.filter = modal;
            $scope.filter.show();
        });
    }
    $scope.closePSFilter = function() {
        $scope.filter.hide();
    }
    $scope.chooseChoicePS = function(choice) {
        // console.log(choice);
        $scope.filter_choice = choice;


    }

    $scope.openPrivilegesSearch = function() {
        $scope.redirect('privileges_search');
    }

})

.controller('PrivilegesSearchCtrl', function($scope, $stateParams, $state, $ionicPopup, Privileges, $ionicModal) {
    $scope.privilege1 = [];

    $scope.privilege1 = Privileges.getPrivilege1();
    // console.log($scope.privilege1);
    $scope.menu_select = 'privilege1';
    $scope.filter_choice = "All";

    $scope.openPSFilter = function() {
        $ionicModal.fromTemplateUrl('templates/privileges_search_filter.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.filter = modal;
            $scope.filter.show();
        });
    }
    $scope.closePSFilter = function() {
        $scope.filter.hide();
    }
    $scope.chooseChoicePS = function(choice) {
        // console.log(choice);
        $scope.filter_choice = choice;


    }

    $scope.search = {};
    $scope.search.ps_keyword = '';
    $scope.search_privileges = function() {
        console.log($scope.search.ps_keyword);

        var tmp_privileges = Privileges.getPrivilege1();
        $scope.privilege1 = [];
        for (var i = 0; i < tmp_privileges.length; i++) {
            console.log(tmp_privileges[i].shop);
            if ($scope.search.ps_keyword == '') {
                $scope.privilege1.push(tmp_privileges[i]);
            } else if (tmp_privileges[i].shop.toLowerCase().includes($scope.search.ps_keyword.toLowerCase())) {
                $scope.privilege1.push(tmp_privileges[i]);
            }
        }
        console.log($scope.privilege1);
    }

})




.controller('DashboardCtrl', function($scope, $stateParams, $state, $ionicPopup, $ionicModal, Login, Dashboard, $ionicScrollDelegate, IUManager, $window, IonSkyManager) {

        $scope.dashboard = {};
        $scope.totalskypass = 0;
        $scope.dashboard.greeting_msg = 'Good Morning';

        $scope.active_iu_exist = true;

        $scope.ionsky = true;

        // console.log($scope.dashboard);
        $scope.login_details = Login.getProfile();
        console.log($scope.login_details);
        $scope.$on('$ionicView.enter', function() {
            $scope.totalskypass = 0;
            // $scope.dashboard = Dashboard.getUserProfile();
            $scope.login_details = Login.getProfile();

            // console.log($scope.login_details.session_id);

            // console.log($scope.login_details.session_id);
            // $scope.qrcode.makeCode($scope.login_details.session_id);
            if ($scope.login_details && $scope.login_details.nric && $scope.login_details.nric != "") {

                $scope.refreshUserProfile()
            } else {
                //                $state.go('app.login');
                $scope.rewards();
            }


        })

        $scope.elById = function(id) {
            return window.document.getElementById(id);
        }

        $scope.valById = function(id) {
            var el = $scope.elById(id);
            return el && el.value;
        }

        $scope.createVirtualCard = function(content) {
            var options = {
                render: 'image',
                crisp: true,
                ecLevel: 'H',
                minVersion: 1,

                fill: "#000000",
                back: "#FFFFFF",

                text: content,
                size: window.innerWidth * 0.8,
                rounded: 0,
                quiet: 1,

                mode: 'image',

                mSize: 30,
                mPosX: 50,
                mPosY: 50,

                label: "",
                fontname: "Arial",
                fontcolor: "#94107e",

                image: $scope.elById('img-buffer')
            };

            var container = $scope.elById('qrcode');
            var qrcode = kjua(options);
            console.log("-----container-----");
            // console.log(container);
            for (var i = 0; i < container.childNodes.length; i++) {
                var element = container.childNodes[i];
                // console.log(element);
                container.removeChild(element);
            }
            // console.log("-----QR-----");
            // console.log(qrcode);
            if (qrcode) {
                container.appendChild(qrcode);
            }
        }

        $scope.$on('$ionicView.enter', function() {
            console.log("Dashboard ionic view enter");
            console.log($scope.login_details);
            if ($scope.login_details) {
                // console.log("-- Create Virtual Card --");
                // console.log($scope.login_details.session_id);
                // $scope.qrcode = new QRCode(document.getElementById("qrcode"));
                // $scope.qrcode.makeCode("" + $scope.login_details.session_id);
                $scope.createVirtualCard("" + $scope.login_details.session_id);
                localStorage.setItem("session_id", $scope.login_details.session_id);
                // console.log(localStorage.getItem('session_id'));
            }
            // if ($scope.login_details.length != 0) {
            //     $scope.qrcode = new QRCode(document.getElementById("qrcode"));
            //     $scope.qrcode.makeCode($scope.login_details.session_id);
            // }

        })
        $scope.getActiveIU = function() {
            $scope.dashboard.active_iu = IUManager.getIU();
            IUManager.getIUs($scope.dashboard.member_id, $scope.device_type, function($data, $status, $headers, $config) {
                $scope.dashboard.active_iu = IUManager.getIU();
                // console.log($scope.dashboard.active_iu);
                for (var i = 0; i < $scope.dashboard.active_iu.length; i++) {
                    if ($scope.dashboard.active_iu[i].activate == true) {

                        $scope.active_iu_exist = true;
                        break;
                    } else {
                        $scope.active_iu_exist = false;
                    }
                }

                // console.log($scope.dashboard.active_iu);
                $scope.$broadcast('scroll.refreshComplete');


            }, function($data, $status, $headers, $config) {

            })

        }
        $scope.refreshUserProfile = function() {
            $scope.totalskypass = 0;
            $scope.dashboard = Dashboard.getUserProfile();

            Login.getIonSky(function() {
                $scope.ionsky = Login.ionsky.enable;
            });

            Dashboard.refreshUserProfile($scope.device_type, $scope.login_details.nric, function($data, $status, $headers, $config) {
                $scope.dashboard = Dashboard.getUserProfile();
                if ($scope.dashboard.carpark_points[1]) {
                    $scope.cp = $scope.dashboard.carpark_points[0]['points'] + $scope.dashboard.carpark_points[1]['points'];
                } else {
                    $scope.cp = $scope.dashboard.carpark_points[0]['points'];
                }


                var d = new Date();

                if (d.getHours() >= 0 && d.getHours() < 12) {
                    $scope.dashboard.greeting_msg = 'Good Morning';
                } else if (d.getHours() >= 12 && d.getHours() < 18) {
                    $scope.dashboard.greeting_msg = 'Good Afternoon';
                } else {
                    $scope.dashboard.greeting_msg = 'Good Evening';
                }
                // console.log($scope.dashboard);
                // console.log($scope.dashboard.active_iu);
                $scope.getActiveIU();


            }, function($data, $status, $headers, $config, $error) {
                var alertPopup = $ionicPopup.alert({
                    title: '',
                    template: $error,
                    buttons: [{
                        type: 'ion-btn',
                        text: 'OK'
                    }]
                });
            });

            IonSkyManager.getCredits($scope.login_details.card_no, $scope.device_type, function($data, $status, $headers, $config) {
                console.log("Get Credits");
                $passes = $data.data.enovax.data.data;
                for (var i = 0; i < $passes.length; i++) {
                    $scope.totalskypass++;
                }

                console.log($scope.totalskypass);
            }, function($data, $status, $headers, $config) {
                // var alertPopup = $ionicPopup.alert({
                //     title: '',
                //     template: $data.msg,
                //     buttons: [{
                //         type: 'ion-btn',
                //         text: 'OK'
                //     }]
                // });
            });
        }

        // Dashboard.dashboard(function($data, $status, $headers, $config) {
        //         $scope.dashboard = {};
        //         $scope.dashboard = Dashboard.getData();
        //    }, function($data, $status, $headers, $config){
        //       var alertPopup = $ionicPopup.alert({
        //            title: 'Error',
        //            template: $data.msg,
        //            buttons:[{
        //               type: 'ion-btn',
        //               text: 'OK'
        //            }]
        //         });
        //    })




        $ionicModal.fromTemplateUrl('templates/dashboardmenu.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.d_menu = modal;
            // $scope.legends.show();
        });

        $scope.showMenu = function() {
            // console.log('showMenu');
            $scope.d_menu.show();
        }
        $scope.hideMenu = function() {
            $scope.d_menu.hide();
        }
        $scope.logout = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Logout',
                template: 'Do you want to logout?',
                okType: 'ion-btn'
            });

            confirmPopup.then(function(res) {
                if (res) {
                    //                    $window.localStorage.setItem('logindetail') = [];
                    $window.localStorage.removeItem('logindetail');
                    localStorage.removeItem('session_id');
                    $scope.loginData = {};
                    $scope.profile = {};
                    // JC Check Here
                    if (typeof(cordova) !== 'undefined') {
                        cordova.plugins.IonGss.auth("");
                        cordova.plugins.IonGss.tabBar("0");
                    }
                    $scope.login_details = {};
                    $scope.dashboard = {};

                    $scope.hideMenu();
                    // $scope.rewards();
                    $state.go('app.login');
                    Login.setProfile([]);
                    Dashboard.setUserProfile([]);
                } else {
                    $scope.hideMenu();
                }
            });
        }
        $scope.doRefresh = function() {


            $scope.dashboard = Dashboard.getUserProfile();
            $scope.refreshUserProfile();
        }

        $scope.scrolltoQR = function() {
            $ionicScrollDelegate.$getByHandle('mainScroll').scrollBottom(true);
        }
    })
    .controller('IUManagerCtrl', function($scope, $stateParams, $state, $ionicPopup, IUManager, Login) {
        $scope.profile = Login.getProfile();

        $scope.checkIU = function() {

            $scope.iu_wrong = false;
            if ($scope.new_iu.confirm_iu_no != $scope.new_iu.iu_no) {

                $scope.iu_wrong = true;
            } else {

                $scope.iu_wrong = false;
            }
        }

        $scope.disable_save = true;
        $scope.btn_enable = function(iu) {
            if ($scope.iu.active_iu != iu) {
                $scope.disable_save = false;
            } else {
                $scope.disable_save = true;
            }

        }


        $scope.iu_menu_selected = 'set_delete_active_iu';

        $scope.changeIUMenu = function(menu_id) {
            $scope.iu_menu_selected = menu_id;
            if ($scope.profile.tier != "The 100" && $scope.profile.tier != "ION Privi Elite") {
                $scope.getActiveIU();
            } else {
                $scope.getMutipleActiveIU();
            }
        };

        $scope.iu = [];
        if ($scope.profile.tier != 'The 100' && $scope.profile.tier != "ION Privi Elite") {
            $scope.iu.active_iu = '';

        } else {
            $scope.iu.active_iu = [];
        }



        $scope.iu = IUManager.getIU();
        IUManager.getIUs($scope.profile.member_id, $scope.device_type, function($data, $status, $headers, $config) {
            $scope.iu = IUManager.getIU();

            if ($scope.profile.tier != 'The 100' && $scope.profile.tier != "ION Privi Elite") {
                $scope.getActiveIU();
            } else {
                $scope.getMutipleActiveIU();
            }

        }, function($data, $status, $headers, $config) {

        })

        $scope.enable_save_btn = false;
        $scope.getActiveIU = function() {
            $scope.iu.active_iu = IUManager.getActiveIU();
            // console.log($scope.iu.active_iu);
            $scope.iu.active_selected = $scope.iu.active_iu;
            if ($scope.iu.active_selected) {
                $scope.enable_save_btn = true;
            }
        }
        $scope.getMutipleActiveIU = function() {
            $scope.iu.active_iu = IUManager.getMultipleActiveIU();
            console.log($scope.iu.active_iu.length);
            if ($scope.iu.active_iu.length > 0) {
                $scope.enable_save_btn = true;
            }
        }

        $scope.set_selected_iu = function(iu_no) {

            $scope.iu.active_selected = iu_no;
            if ($scope.iu.active_selected) {
                $scope.enable_save_btn = true;
            }
        }
        $scope.set_mutiple_selected_iu = function(index) {
            console.log($scope.iu[index]['activate']);
            if ($scope.iu[index]['activate'] == false) {
                $scope.iu[index]['activate'] = true;
            } else {
                $scope.iu[index]['activate'] = false;
            }
            // console.log($scope.iu[index]['activate']);
            $scope.enable_save_btn = false;
            for (var i = 0; i < $scope.iu.length; i++) {
                if ($scope.iu[i]['activate']) {
                    $scope.enable_save_btn = true;
                    break;
                }
            }
        }

        // $scope.iu.member_passsword = null;
        $scope.disable_done = false;
        $scope.checkPwd = function() {

            if ($scope.iu.member_passsword == null || $scope.iu.member_passsword.toString().length < 6) {
                $scope.disable_done = true;
            } else {
                $scope.disable_done = false;
            }
            // console.log($scope.disable_done);
        }

        $scope.set_iu_as_active = function() {
            // $scope.hideKeyboard();
            $scope.showError = false;
            $ionicPopup.show({
                cssClass: 'iu-popup iu-popup-active',
                template: '<input type="tel" class="pwd" pattern="[0-9]*" inputmode="numeric" ng-model="iu.member_passsword" limit-to="6" ng-change="checkPwd();showError=false" placeholder="Enter password to proceed"> <p ng-if="showError" class="form-errors">This field is required.</p><p ng-if="iu.member_passsword && iu.member_passsword.length < 6" class="form-errors">Minimum length 6 digits.</p>',
                title: 'Activate/Deactivate IU',
                subTitle: $scope.iu.active_selected,
                scope: $scope,
                buttons: [{
                        text: 'Done',
                        type: 'button-positive',

                        onTap: function(e) {


                            if (!$scope.iu.member_passsword) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                                $scope.showError = true;
                            } else if ($scope.iu.member_passsword.toString().length < 6) {
                                e.preventDefault();

                            } else {
                                // console.log($scope.iu);
                                if ($scope.profile.tier != 'The 100' && $scope.profile.tier != "ION Privi Elite") {
                                    for (var i = 0; i < $scope.iu.length; i++) {
                                        // console.log('123');
                                        if ($scope.iu[i].iu_no != $scope.iu.active_selected) {
                                            $scope.iu[i].activate = false
                                        } else {
                                            $scope.iu[i].activate = true
                                        }
                                    }
                                }

                                // call update active iu service
                                IUManager.activateIU($scope.device_type, $scope.profile.nric, $scope.profile.member_id, $scope.iu, $scope.iu.member_passsword, function($data, $status, $headers, $config) {
                                    $scope.iu.member_passsword = "";

                                    $scope.iu = IUManager.getIU();
                                    IUManager.getIUs($scope.profile.member_id, $scope.device_type, function($data, $status, $headers, $config) {
                                        $scope.iu = IUManager.getIU();
                                        if ($scope.profile.tier != 'The 100' && $scope.profile.tier != "ION Privi Elite") {
                                            $scope.getActiveIU();
                                        } else {
                                            $scope.getMutipleActiveIU();
                                        }

                                    }, function($data, $status, $headers, $config) {

                                    })

                                    if ($scope.profile.tier != 'The 100' && $scope.profile.tier != "ION Privi Elite") {
                                        $ionicPopup.show({
                                            title: '',
                                            template: 'IU ' + $scope.iu.active_selected + '<br/>has been selected.',
                                            scope: $scope,
                                            buttons: [{
                                                text: 'OK',
                                                type: 'ion-btn'
                                            }]
                                        });
                                    } else {
                                        var iu_1 = '';
                                        var iu_2 = '';
                                        var iu_3 = '';
                                        if ($scope.iu.length != 0) {
                                            if ($scope.iu[0].activate == true) {
                                                iu_1 = 'IU ' + $scope.iu[0].iu_no + '<br/>';
                                            }
                                            if ($scope.iu[1]) {
                                                if ($scope.iu[1].activate == true) {
                                                    iu_2 = 'IU ' + $scope.iu[1].iu_no + '<br/>';
                                                }
                                            }
                                            if ($scope.iu[2]) {
                                                if ($scope.iu[2].activate == true) {
                                                    iu_3 = 'IU ' + $scope.iu[2].iu_no + '<br/>';

                                                }
                                            }

                                            $ionicPopup.show({
                                                title: '',
                                                template: iu_1 + iu_2 + iu_3 + 'have been selected.',
                                                scope: $scope,
                                                buttons: [{
                                                    text: 'OK',
                                                    type: 'ion-btn'
                                                }]
                                            });
                                        }

                                    }

                                }, function($data, $status, $headers, $config) {
                                    $scope.iu.member_passsword = "";
                                    $ionicPopup.show({
                                        title: '',
                                        template: $data.msg,
                                        scope: $scope,
                                        buttons: [{
                                            text: 'OK',
                                            type: 'ion-btn'
                                        }]
                                    });

                                })


                                return $scope.iu.member_passsword;
                            }
                        }
                    },
                    { text: 'Cancel' }
                ]
            });
        };


        $scope.remove_iu = function(iu_no, index) {
            // $scope.hideKeyboard();
            $scope.showRequiredError = false;


            $ionicPopup.show({
                cssClass: 'iu-popup iu-popup-delete',
                template: '<input type="tel" ng-model="iu.member_passsword" placeholder="Enter password to delete" class="pwd" pattern="[0-9]*" ng-change="showRequiredError = false;" limit-to="6" inputmode="numeric"><p ng-if="showRequiredError" class="form-errors">This field is required.</p><p ng-if="iu.member_passsword && iu.member_passsword.length < 6" class="form-errors">Minimum length 6 digits.</p>',
                title: '',
                subTitle: iu_no,
                scope: $scope,
                buttons: [{
                        text: 'Done',
                        type: 'button-positive',
                        onTap: function(e) {

                            if (!$scope.iu.member_passsword) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                                $scope.showRequiredError = true;
                            } else if ($scope.iu.member_passsword && $scope.iu.member_passsword.length < 6) {
                                e.preventDefault();

                            } else {
                                // call delete iu service

                                $scope.iu.splice(index, 1);
                                IUManager.activateIU($scope.device_type, $scope.profile.nric, $scope.profile.member_id, $scope.iu, $scope.iu.member_passsword, function($data, $status, $headers, $config) {

                                    $scope.iu.member_passsword = "";

                                    $scope.iu = IUManager.getIU();
                                    IUManager.getIUs($scope.profile.member_id, $scope.device_type, function($data, $status, $headers, $config) {
                                        $scope.iu = IUManager.getIU();

                                        if ($scope.profile.tier != 'The 100' && $scope.profile.tier != 'ION Privi Elite') {
                                            $scope.getActiveIU();

                                        } else {
                                            $scope.getMutipleActiveIU();
                                        }

                                    }, function($data, $status, $headers, $config) {

                                    })
                                    $ionicPopup.show({
                                        title: '',
                                        template: 'IU ' + iu_no + '<br/>has been deleted.',
                                        scope: $scope,
                                        buttons: [{
                                            text: 'OK',
                                            type: 'ion-btn'
                                        }]
                                    });
                                }, function($data, $status, $headers, $config) {
                                    $scope.iu = IUManager.getIU();
                                    IUManager.getIUs($scope.profile.member_id, $scope.device_type, function($data, $status, $headers, $config) {
                                        $scope.iu = IUManager.getIU();

                                        if ($scope.profile.tier != 'The 100' && $scope.profile.tier != 'ION Privi Elite') {
                                            $scope.getActiveIU();

                                        } else {
                                            $scope.getMutipleActiveIU();
                                        }

                                    }, function($data, $status, $headers, $config) {

                                    })
                                    $scope.iu.member_passsword = "";
                                    $ionicPopup.show({
                                        title: '',
                                        template: $data.msg,
                                        scope: $scope,
                                        buttons: [{
                                            text: 'OK',
                                            type: 'ion-btn'
                                        }]
                                    });

                                })

                                return $scope.iu.member_passsword;
                            }
                        }
                    },
                    { text: 'Cancel' }
                ]
            });
        };

        $scope.new_iu = [];
        $scope.new_iu.is_active_iu = false;
        // $scope.new_iu.iu_no = null;
        // $scope.new_iu.confirm_iu_no = null;
        // $scope.new_iu.confirm_password = null;
        // $scope.new_iu.is_active_iu = 0;

        $scope.new_iu_submit = function() {
            // $scope.hideKeyboard();
            if ($scope.iu.length < 3) {

                var str = $scope.new_iu.iu_no.toString();

                if (str.length < 10) {

                    $ionicPopup.show({
                        title: '',
                        template: 'Invalid IU.',
                        scope: $scope,
                        buttons: [{
                            text: 'OK',
                            type: 'ion-btn'
                        }]
                    });
                } else {
                    // console.log('ffff');
                    console.log($scope.new_iu.is_active_iu);
                    var max_activate_iu = 1;
                    if ($scope.profile.tier == 'The 100' || $scope.profile.tier == 'ION Privi Elite') {
                        max_activate_iu = 3;
                    }

                    var activated_count = 0;
                    if ($scope.new_iu.is_active_iu) {
                        for (var i = 0; i < $scope.iu.length; i++) {
                            if (max_activate_iu == 1) {
                                $scope.iu[i].activate = false
                            } else {
                                if (activated_count < max_activate_iu) {
                                    activated_count++;
                                } else {
                                    $scope.iu[i].activate = false
                                }
                            }
                        }
                    }


                    $scope.iu.push({ 'iu_no': $scope.new_iu.iu_no, 'activate': $scope.new_iu.is_active_iu });
                    // for (var i = 0; i < $scope.iu.length; i++) {
                    //     if ($scope.iu[i].iu_no != $scope.new_iu.iu_no) {
                    //         $scope.iu[i].activate = false;

                    //     } else {
                    //         $scope.iu[i].activate = true;

                    //     }
                    // }
                    // console.log($scope.iu);
                    // call create iu service
                    IUManager.activateIU($scope.device_type, $scope.profile.nric, $scope.profile.member_id, $scope.iu, $scope.new_iu.confirm_password, function($data, $status, $headers, $config) {


                        $scope.iu = IUManager.getIU();

                        IUManager.getIUs($scope.profile.member_id, $scope.device_type, function($data, $status, $headers, $config) {
                            $scope.iu = IUManager.getIU();
                            console.log($scope.iu);
                            if ($scope.profile.tier != "The 100" && $scope.profile.tier != "ION Privi Elite") {
                                $scope.getActiveIU();
                            } else {
                                $scope.getMutipleActiveIU();
                            }

                        }, function($data, $status, $headers, $config) {

                        })
                        var myPopup = $ionicPopup.show({
                            title: '',
                            template: 'IU ' + $scope.new_iu.iu_no + '<br/>has been added.',
                            scope: $scope,
                            buttons: [{
                                text: 'OK',
                                type: 'ion-btn'
                            }]
                        });

                        myPopup.then(function(res) {
                            $scope.new_iu = [];
                        });

                    }, function($data, $status, $headers, $config) {
                        $scope.iu.member_passsword = "";
                        $scope.iu = IUManager.getIU();

                        IUManager.getIUs($scope.profile.member_id, $scope.device_type, function($data, $status, $headers, $config) {
                            $scope.iu = IUManager.getIU();
                            // console.log($scope.iu);
                            if ($scope.profile.tier != "The 100" && $scope.profile.tier != "ION Privi Elite") {
                                $scope.getActiveIU();
                            } else {
                                $scope.getMutipleActiveIU();
                            }

                        }, function($data, $status, $headers, $config) {

                        })
                        $ionicPopup.show({
                            title: '',
                            template: $data.msg,
                            scope: $scope,
                            buttons: [{
                                text: 'OK',
                                type: 'ion-btn'
                            }]
                        });

                    })
                }


            } else {
                $ionicPopup.show({
                    title: '',
                    template: "You have reached the max. number of pre-registered IUs.",
                    scope: $scope,
                    buttons: [{
                        text: 'OK',
                        type: 'ion-btn'
                    }]
                });
            }


        }



    })
    .controller('EditProfileCtrl', function($scope, Register, EditProfile, IUManager, Login, $stateParams, $state, $ionicPopup, $ionicLoading) {

        $scope.editProfileData = Login.getProfile();

        $scope.editProfileData.contact = parseInt($scope.editProfileData.contact);
        // console.log($scope.editProfileData.dob);
        // $scope.editProfileData.dob = moment($scope.editProfileData.dob, "YYYY/MM/DD");
        $scope.editProfileData.dob = new Date($scope.editProfileData.dob);
        // console.log($scope.editProfileData.dob);
        if ($scope.editProfileData.unit == "null") {
            $scope.editProfileData.unit = "";
        }

        $scope.$on('$ionicView.loaded', function() {
            console.log('edit profile view loaded');
            $ionicLoading.show();

            $scope.all_countries = Register.getCountry();

            $scope.all_nationalities = Register.getNationality();
            $scope.all_salutation = Register.getSalutation();
            console.log('edit profile view loaded1');

            //     Register.retrieveCountry($scope.device_type, function($data, $status, $headers, $config) {
            //         $scope.all_countries = Register.getCountry();
            //         console.log('edit profile view loaded2');

            //         Register.retrieveNationality($scope.device_type, function($data, $status, $headers, $config) {
            //             $scope.all_nationalities = Register.getNationality();
            //             console.log('edit profile view loaded3');

            //             Register.retrieveSalutation($scope.device_type, function($data, $status, $headers, $config) {
            //                 $scope.all_salutation = Register.getSalutation();
            //                 console.log('edit profile view loaded4');
            //                 // $ionicLoading.hide();


            //             }, function($data, $status, $headers, $config) {
            //                 // $ionicLoading.hide();
            //             })
            //         }, function($data, $status, $headers, $config) {
            //             // $ionicLoading.hide();
            //         })
            //     }, function($data, $status, $headers, $config) {
            //         // $ionicLoading.hide();
            //     })

            //     console.log('edit profile view loaded5');

            // $scope.$on('$ionicView.enter', function() {
            //     console.log('view enter');

            // })

            $scope.all_countries = Register.getCountry();
            $scope.all_nationalities = Register.getNationality();
            $scope.all_salutation = Register.getSalutation();


            $scope.ius = IUManager.getIU();
            IUManager.getIUs($scope.editProfileData.member_id, $scope.device_type, function($data, $status, $headers, $config) {
                $scope.ius = IUManager.getIU();
                // console.log($scope.ius);
                console.log('edit profile view loaded6');

            }, function($data, $status, $headers, $config) {

            })
            console.log('edit profile view loaded6');
        })



        $scope.submitProfile = function(form) {
            // console.log($scope.editProfileData);

            if (!form.$invalid) {
                if ($scope.editProfileData.country != '') {
                    form.$submitted = false;
                    EditProfile.editProfile($scope.editProfileData, $scope.device_type, $scope.editProfileData.card_no, function($data, $status, $headers, $config) {
                        Login.setProfile($data.data);
                        $scope.editProfileData = $data.data;
                        // $scope.editProfileData.dob = moment($data.data.dob).format('DD/MM/YYYY')
                        $scope.editProfileData.dob = new Date($data.data.dob);
                        // console.log($scope.editProfileData.dob);
                        var alertPopup = $ionicPopup.alert({
                            title: '',
                            template: "Profile has been updated.",
                            buttons: [{
                                type: 'ion-btn',
                                text: 'OK'
                            }]
                        });
                    }, function($data, $status, $headers, $config) {
                        var alertPopup = $ionicPopup.alert({
                            title: '',
                            template: $data.msg,
                            buttons: [{
                                type: 'ion-btn',
                                text: 'OK'
                            }]
                        });
                    })
                } else {
                    var alertPopup = $ionicPopup.alert({
                        title: '',
                        template: "Please choose your country.",
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                }

            }
        }

    })
    .controller('TotalPointsCtrl', function($scope, Register, EditProfile, Login, Dashboard, $stateParams, $state, $ionicPopup) {
        $scope.doRefreshTotalPoints = function() {
            $scope.$broadcast('scroll.refreshComplete');
            $scope.last_updated_time = moment().format('DD MMM YYYY HH:mm');
        };

        $scope.dashboard = Dashboard.getUserProfile();

        $scope.points = Login.getProfile();
        // console.log($scope.points);
        $scope.points.ion_points_curr_cycle = $scope.dashboard.ion_points_curr_cycle;
        $scope.points.ion_points_prev_cycle = $scope.dashboard.ion_points_prev_cycle;
        $scope.points.ion_points_prev_expiry = new Date($scope.points.ion_points_prev_expiry);
        $scope.points.ion_points_curr_expiry = new Date($scope.dashboard.ion_points_curr_expiry);
        $scope.points.carpark_points = $scope.dashboard.carpark_points;




    })

.controller('CarparkDollarCtrl', function($scope, CarparkDollar, Dashboard, Login, $stateParams, $state, $ionicPopup, IUManager) {
        $scope.transfer_points = [];
        $scope.transfer_points.iu_no = "";
        $scope.profile = Login.getProfile();
        $scope.dashboard = Dashboard.getUserProfile();
        $scope.profile.ion_points_curr_cycle = $scope.dashboard.ion_points_curr_cycle;
        $scope.profile.ion_points_prev_cycle = $scope.dashboard.ion_points_prev_cycle;

        if ($scope.dashboard.carpark_points[1]) {
            $scope.cp = $scope.dashboard.carpark_points[0]['points'] + $scope.dashboard.carpark_points[1]['points'];
        } else {
            $scope.cp = $scope.dashboard.carpark_points[0]['points'];
        }

        // $scope.points.ion_points_curr_cycle = Dashboard.
        // console.log($scope.dashboard);
        // console.log($scope.profile.carpark_points[0].points);

        $scope.ius = IUManager.getIU();
        IUManager.getIUs($scope.profile.member_id, $scope.device_type, function($data, $status, $headers, $config) {
            $scope.ius = IUManager.getIU();
            // console.log($scope.ius);
            for (var i = 0; i < $scope.ius.length; i++) {
                if ($scope.ius[i].activate == true) {
                    $scope.transfer_points.iu_no = $scope.ius[i].iu_no;
                    break;
                } else {
                    $scope.transfer_points.iu_no = "";
                }
            }
        }, function($data, $status, $headers, $config) {

        })

        $scope.doTransfer = function(form) {

            if (!form.$invalid) {
                CarparkDollar.convertCarparkDollar($scope.device_type, $scope.profile.nric, $scope.profile.card_no, $scope.transfer_points, function($data, $status, $headers, $config) {
                    $scope.profile.ion_points_curr_cycle = parseInt($scope.profile.ion_points_curr_cycle) - parseInt($scope.transfer_points.points);
                    $scope.transfer_points.points = '';
                    $scope.transfer_points.password = '';
                    console.log($data.data.carpark_points[0]['points']);
                    console.log($data.data.carpark_points[1]['points']);
                    $scope.cp = parseFloat($data.data.carpark_points[0]['points']) + parseFloat($data.data.carpark_points[1]['points']);
                    // console.log($scope.profile.ion_points_curr_cycle);
                    // console.log($scope.transfer_points.points);

                    // console.log($scope.profile.ion_points_curr_cycle);
                    // $scope.points.ion_points_curr_cycle  = $scope.points.ion_points_curr_cycle - $scope.transfer_points.points;

                    form.$submitted = false;
                    var alertPopup = $ionicPopup.alert({
                        title: '',
                        template: 'Points have been transferred.',
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                }, function($data, $status, $headers, $config) {
                    var alertPopup = $ionicPopup.alert({
                        title: '',
                        template: $data.msg,
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                })
            }

        }


    })
    .controller('RewardsTncCtrl', function($scope, $stateParams, $state, $ionicPopup, TermsCondition) {
        $scope.tnc = [];

        // $scope.tnc = Login.getProfile();
        // console.log($scope.profile.carpark_points[0].points);

        $scope.tnc = TermsCondition.getTnc();
        TermsCondition.loadTerms(function($data, $status, $headers, $config) {
            $scope.tnc = TermsCondition.getTnc();


        }, function($data, $status, $headers, $config) {

        })


    })


.controller('ScanReceiptsCtrl', function($scope, ScanReceipt, $ionicHistory, $stateParams, $state, $ionicPopup, $cordovaCapture, $ionicPopover, $ionicLoading, $cordovaImagePicker, $ionicModal, $cordovaFileTransfer, $ionicScrollDelegate, $location, Login, $filter, $ionicPlatform, $timeout, $cordovaCamera) {

    $scope.receipts = [];
    $scope.currentCapture = [];
    $scope.stores = [];
    $scope.selectedStore = "";

    $scope.is_camera_off = false;
    $scope.showResult = false;

    $scope.hide_footer = false;
    $scope.show_preview = false;

    $scope.pictimestamp = (new Date()).getTime();

    $scope.scrollToAnchor = function(id) {
        // console.log(id);
        $location.hash('header-' + id);
        $timeout(function() {
            $ionicScrollDelegate.anchorScroll(true);
        }, 1)
    };

    $scope.$on('$ionicView.enter', function() {
        $scope.profile = Login.getProfile();
        console.log($scope.profile);

        if (typeof($scope.profile) === 'undefined') {
            if ($state.current.name == "app.scan_receipts") {
                localStorage.setItem("last_login_view", "ScanReceipts");
            }
            $scope.lastView = $ionicHistory.backView();
            $scope.redirect('backtoroot');
        }

        $ionicPopover.fromTemplateUrl('templates/store.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(popover) {
            $scope.popover2 = popover;
        });

        // console.log("asdsa asdasd");
        $scope.stores = ScanReceipt.getStoreList();
        ScanReceipt.shoplist($scope.device_type, function($data, $status, $headers, $config) {

            $scope.stores = ScanReceipt.getStoreList();
            // console.log($scope.stores);
            $scope.getFirstChar();
        }, function() {});
    });


    $scope.selectStore = function(store) {
        $scope.selectedStore = store;
        $ionicPlatform.offHardwareBackButton(function(event) {
            // alert('abc');
            event.preventDefault();
        }, 100);
        $scope.closeStore();
        $scope.captureImage();
    }

    $scope.startOver = function() {
        $scope.receipts = [];
        $scope.clearScanReceipt();
        $scope.showResult = false;
    }

    $scope.openImg = function() {
        $ionicModal.fromTemplateUrl('templates/samplereceipt.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
            $scope.modal.show();
        });
    }
    $scope.closeImg = function() {
        console.log("closeImg");
        $scope.modal.hide();
    }

    $scope.getFirstChar = function() {
        $scope.firstChars = [];
        $scope.names = [];

        // for (var i = 48; i <= 57; i++) {


        var tmp_arr = [];
        for (var j = 0; j < $scope.stores.length; j++) {
            // console.log($scope.stores[j].outlet_code);
            if ($scope.stores[j].outlet_code.charCodeAt(0) >= 48 && $scope.stores[j].outlet_code.charCodeAt(0) <= 57) {
                // console.log(String.fromCharCode(i));
                tmp_arr.push($scope.stores[j]);
            }
        }

        // console.log(String.fromCharCode(i));
        // console.log(tmp_arr);

        // if (tmp_arr.length == 0)  continue;

        // $scope.names = Object.assign($scope.names, JSON.parse('{"' + String.fromCharCode(i) + '": '+JSON.stringify(tmp_arr)+'}'));

        // console.log(String.fromCharCode(i));
        $scope.firstChars.push('#');
        $scope.names.push(tmp_arr);
        console.log($scope.names);
        // }
        for (var i = 65; i <= 90; i++) {


            var tmp_arr = [];
            for (var j = 0; j < $scope.stores.length; j++) {
                // console.log($scope.stores[j].outlet_code);
                if ($scope.stores[j].outlet_code.charAt(0) == String.fromCharCode(i)) {
                    // console.log(String.fromCharCode(i));
                    tmp_arr.push($scope.stores[j]);
                }
            }

            // console.log(String.fromCharCode(i));
            // console.log(tmp_arr);

            if (tmp_arr.length == 0) continue;

            // $scope.names = Object.assign($scope.names, JSON.parse('{"' + String.fromCharCode(i) + '": '+JSON.stringify(tmp_arr)+'}'));

            // console.log(String.fromCharCode(i));
            $scope.firstChars.push(String.fromCharCode(i));
            $scope.names.push(tmp_arr);
        }
    }

    $scope.captureImage = function() {
        $ionicPlatform.offHardwareBackButton(function(event) {
            // alert('999');
            event.preventDefault();
        }, 100);
        $scope.openScanReceipt();
        $scope.startCamera();
    }

    $scope.openStore = function() {
        $scope.popover2.show();
    }
    $scope.closeStore = function() {
        $ionicPlatform.offHardwareBackButton(function(event) {
            // alert('3');
            event.preventDefault();
        }, 100);
        $scope.popover2.hide();
    }

    $scope.$on('$ionicView.leave', function() {
        if ($scope.popover2) {
            $scope.popover2.remove();
        }
    });

    $scope.submitReceipt = function(success, failure) {
        ScanReceipt.submitReceipt($scope.profile.card_no, $scope.profile.nric, $scope.receipts, function() {
            $scope.receipts = [];
            $scope.clearScanReceipt();
            $scope.showResult = true;
        }, function() {
            var alertPopup = $ionicPopup.alert({
                title: '',
                template: 'There is an error in the submission. Please approach concierge counter',
                buttons: [{
                    type: 'ion-btn',
                    text: 'OK'
                }]
            });
        }, $ionicPopup);
    }

    $scope.showReceipts = function(receipt) {
        console.log(receipt);
    }

    $scope.deleteCaptureImage = function(filepath) {
        console.log("Starting to write the file :3");
        var workerFolder = "";
        if (ionic.Platform.isIOS()) {
            workerFolder = cordova.file.tempDirectory;
        } else if (ionic.Platform.isAndroid()) {
            workerFolder = cordova.file.applicationStorageDirectory;
        }
        console.log(workerFolder);
        var filename = filepath.split("/").pop();
        window.resolveLocalFileSystemURL(workerFolder, function(dir) {
            dir.getFile(filename, { create: false }, function(fileEntry) {
                fileEntry.remove(function() {
                    // The file has been removed succesfully
                    console.log("file removed");
                }, function(error) {
                    // Error deleting the file
                    console.log("error! ");
                    console.log(error);
                }, function() {
                    // The file doesn't exist
                });
            });
        });
    }

    $scope.upload = function(success, failure) {
        var url = apiurl + 'upload_receipt.php';
        //File for Upload
        var targetPath = cordova.file.externalRootDirectory + $scope.currentCapture[0];
        if (ionic.Platform.isAndroid()) {
            targetPath = $scope.currentCapture[0];
        }

        console.log("Uploading: " + targetPath);

        var filename = targetPath.split("/").pop();
        // console.log("filename " + filename);
        var options = {
            fileKey: "receipt_1",
            fileName: filename,
            chunkedMode: false,
            mimeType: "image/jpg",
            params: {} // directory represents remote directory,  fileName represents final remote file name
        };

        $ionicLoading.show();
        $cordovaFileTransfer.upload(url, targetPath, options, true).then(function(result) {

            var receipt_response = JSON.parse(result.response);
            if ($scope.currentCapture.length > 1) {
                $scope.deleteCaptureImage($scope.currentCapture[0]);

                //File for Upload
                var targetPath2 = cordova.file.externalRootDirectory + $scope.currentCapture[1];
                if (ionic.Platform.isAndroid()) {
                    targetPath2 = $scope.currentCapture[1];
                }

                console.log("Uploading: " + targetPath2);

                var filename2 = targetPath2.split("/").pop();
                var options = {
                    fileKey: "receipt_1",
                    fileName: filename2,
                    chunkedMode: false,
                    mimeType: "image/jpg",
                    params: { prev_url: receipt_response.data.receipt_image } // directory represents remote directory,  fileName represents final remote file name
                };
                $cordovaFileTransfer.upload(url, targetPath2, options, true).then(function(result2) {
                    $scope.deleteCaptureImage($scope.currentCapture[1]);

                    var receipt_response = JSON.parse(result2.response);
                    console.log($scope.selectedStore);
                    console.log($scope.currentCapture);
                    console.log(receipt_response);
                    $scope.receipts.push({ outlet_code: $scope.selectedStore, current_capture: $scope.currentCapture, receipt_image: receipt_response.data.receipt_image });
                    $scope.clearScanReceipt();
                    $ionicLoading.hide();
                    success();
                }, function(err) {
                    console.log("ERROR: " + JSON.stringify(err));
                    $ionicLoading.hide();
                    failure();
                }, function(progress) {
                    // PROGRESS HANDLING GOES HERE
                });
            } else {
                var receipt_response = JSON.parse(result.response);
                console.log($scope.selectedStore);
                console.log($scope.currentCapture);
                console.log(receipt_response);
                $scope.receipts.push({ outlet_code: $scope.selectedStore, current_capture: $scope.currentCapture, receipt_image: receipt_response.data.receipt_image });
                $scope.clearScanReceipt();
                $ionicLoading.hide();
                success();
            }
        }, function(err) {
            console.log("ERROR: " + JSON.stringify(err));
            $ionicLoading.hide();
            failure();
        }, function(progress) {
            // PROGRESS HANDLING GOES HERE
        });
    }

    // ========
    /**
     * Convert a base64 string in a Blob according to the data and contentType.
     * 
     * @param b64Data {String} Pure base64 string without contentType
     * @param contentType {String} the content type of the file i.e (image/jpeg - image/png - text/plain)
     * @param sliceSize {Int} SliceSize to process the byteCharacters
     * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
     * @return Blob
     */
    $scope.b64toBlob = function(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    /**
     * Create a Image file according to its database64 content only.
     * 
     * @param folderpath {String} The folder where the file will be created
     * @param filename {String} The name of the file that will be created
     * @param content {Base64 String} Important : The content can't contain the following string (data:image/png[or any other format];base64,). Only the base64 string is expected.
     */
    $scope.savebase64AsImageFile = function(folderpath, filename, content, contentType, success, failure) {
        // Convert the base64 string in a Blob
        var DataBlob = $scope.b64toBlob(content, contentType);

        console.log("Starting to write the file :3");
        var workerFolder = "";
        if (ionic.Platform.isIOS()) {
            workerFolder = cordova.file.tempDirectory;
        } else if (ionic.Platform.isAndroid()) {
            workerFolder = cordova.file.applicationStorageDirectory;
        }
        console.log(workerFolder);
        window.resolveLocalFileSystemURL(workerFolder, function(dir) {
            console.log("Access to the directory granted succesfully");
            dir.getFile(filename, { create: true }, function(file) {
                console.log("File created succesfully. " + file.name);
                file.createWriter(function(fileWriter) {
                    console.log("Writing content to file " + file.name);
                    fileWriter.seek(0);
                    fileWriter.truncate(0);
                    fileWriter.write(DataBlob);
                    // alert(cordova.file.tempDirectory, +"/" + file.name);
                    success(file);

                }, function() {
                    // alert('Unable to save file in path ' + folderpath);
                });
            });
        });
    }

    $scope.startCamera = function() {
        CameraPreview.startCamera({ x: 0, y: 0, width: window.screen.width, height: window.screen.height, camera: "back", tapPhoto: true, previewDrag: false, toBack: true });
        CameraPreview.setPreviewSize({ width: window.screen.width, height: window.screen.height });
    }

    $scope.takeFromGallery = function() {
        // if (ionic.Platform.isIOS()) {
        //     workerFolder = cordova.file.tempDirectory;
        // } else if (ionic.Platform.isAndroid()) {
        //     workerFolder = cordova.file.applicationStorageDirectory;
        // }


        // var options = {
        //     maximumImagesCount: 1,
        //     width: 1024,
        //     height: 1024,
        //     quality: 75
        // };

        // $cordovaImagePicker.getPictures(options)
        //     .then(function(results) {
        //         console.log(results);
        //         for (var i = 0; i < results.length; i++) {
        //             console.log('Image URI: ' + results[i]);
        //             $scope.currentCapture.push(results[i]);
        //         }
        //         $scope.closeScanReceiptPopover();
        //     }, function(error) {
        //         console.log(error);
        //         // error getting photos
        //         $scope.closeScanReceiptPopover();
        //     });
        // if (ionic.Platform.isAndroid()){
        CameraPreview.stopCamera();
        $scope.is_camera_off = true;
        // }

        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 1024,
            targetHeight: 1024,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };

        $cordovaCamera.getPicture(options).then(function(imageData) {
            // alert('came in no error');
            // alert("imageData");
            // alert(JSON.stringify(imageData));
            $scope.currentCapture.push(imageData);
            $scope.closeScanReceiptPopover();
        }, function(err) {
            // error
            // alert('error');
            // alert(err);
            $scope.closeScanReceiptPopover();
        });


    }

    $scope.takePicture = function(success, failure) {
        CameraPreview.takePicture(function(imgData) {
            $ionicLoading.show();
            $scope.currentFileName = "ION-" + $scope.receipts.length + "-" + $scope.currentCapture.length + "-" + $filter('date')(new Date(), 'yyyyMMddHHmmss') + ".jpg";
            $scope.savebase64AsImageFile("ION Orchard Receipts", $scope.currentFileName, imgData, "image/jpeg", function(file) {
                var workerFolder = "";
                if (ionic.Platform.isIOS()) {
                    workerFolder = cordova.file.tempDirectory;
                } else if (ionic.Platform.isAndroid()) {
                    workerFolder = cordova.file.applicationStorageDirectory;
                }
                console.log(workerFolder);
                window.resolveLocalFileSystemURL(workerFolder, function(dir) {
                    dir.getFile($scope.currentFileName, { create: false }, function(file) {

                        $scope.currentCapture.push(workerFolder + "/" + $scope.currentFileName);
                        if ($scope.currentCapture.length >= 2) {
                            $scope.closeScanReceiptPopover();
                        }
                        $ionicLoading.hide();
                    });
                }, function() {
                    $ionicLoading.hide();
                    // alert("No receipt file!");
                });
            });
        });
    }

    $ionicPopover.fromTemplateUrl('templates/take_receipt.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover = popover;
    });

    $scope.openScanReceipt = function() {
        $ionicPlatform.registerBackButtonAction(function(event) {
            $scope.closeScanReceipt();
            event.preventDefault();
        }, 500);
        if (ionic.Platform.isAndroid()) {
            var permissions = cordova.plugins.permissions;
            permissions.checkPermission(permissions.CAMERA, function(status) {
                // alert(JSON.stringify(status));
                if (status.hasPermission) {
                    console.log("Yes :D ");
                } else {
                    console.warn("No :( ");
                    var alertPopup = $ionicPopup.alert({
                        title: "",
                        template: "Please allow permission to access camera.",
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                    alertPopup.then(function(res) {
                        $scope.closeScanReceipt();
                        $scope.closeScanReceiptPopover();

                        $state.go('app.scan_receipts');
                    });

                }
            });
        }
        $scope.currentCapture = [];
        $scope.hideNavView();
        $scope.popover.show();
        $scope.hide_footer = false;
        $scope.show_preview = false;

    };

    $scope.closeScanReceipt = function() {
        $scope.showNavView();
        if (!$scope.is_camera_off) {
            CameraPreview.hide();
            CameraPreview.stopCamera();
        }

        $scope.popover.hide();
    }

    $scope.closeScanReceiptPopover = function() {
        $scope.pictimestamp = (new Date()).getTime();
        if ($scope.currentCapture.length > 0) {
            $scope.hide_footer = true;
            $scope.show_preview = true;
        } else {
            $scope.clearCloseScanReceiptPopover();
        }
    };

    $scope.clearScanReceipt = function() {
        for (i = 0; i < $scope.currentCapture.length; i++) {
            $scope.deleteCaptureImage($scope.currentCapture[i]);
        }
        $scope.currentCapture = [];
    }

    $scope.clearCloseScanReceiptPopover = function() {
        $scope.clearScanReceipt();
        $scope.closeScanReceipt();
    };


    $scope.noUseScanReceipt = function() {
        // if(ionic.Platform.isAndroid()){
        $scope.startCamera();
        // }
        $scope.hide_footer = false;
        $scope.show_preview = false;
        $scope.clearScanReceipt();
    }

    $scope.useScanReceipt = function() {
        $scope.upload(function() {
            // $scope.hide_footer = false;
            // $scope.show_preview = false;
            $scope.clearCloseScanReceiptPopover();
        }, function() {
            $scope.hide_footer = false;
            $scope.show_preview = false;
            $scope.clearCloseScanReceiptPopover();
        });
    }

    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.popover.remove();
        if ($scope.modal) {
            $scope.modal.remove();
        }
    });

    $scope.checkGalleryPermission = function() {
        var permissions = cordova.plugins.permissions;
        permissions.checkPermission(permissions.READ_EXTERNAL_STORAGE, function(status) {
            if (status.hasPermission) {
                console.log("Yes :D ");
            } else {
                console.warn("No :( ");
                permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE, function(status) {
                    console.warn("Status? " + status.hasPermission);
                }, function() {
                    console.warn("No Permission ");
                });
            }
        });
    }

    $scope.$on('$ionicView.loaded', function() {
        if (ionic.Platform.isAndroid()) {
            var permissions = cordova.plugins.permissions;
            permissions.checkPermission(permissions.CAMERA, function(status) {
                if (status.hasPermission) {
                    console.log("Yes :D ");
                } else {
                    console.warn("No :( ");
                    permissions.requestPermission(permissions.CAMERA, function(status) {
                        console.warn("Status? " + status.hasPermission);
                        $scope.checkGalleryPermission();
                    }, function() {
                        console.warn("No Permission ");
                        $scope.checkGalleryPermission();
                    });
                }
            });
        }
    });

})

.controller('IonSkyCtrl', function($scope, $stateParams, $state, $ionicHistory, $ionicPopup, IonSkyManager, Login, $interval, $ionicLoading, $filter) {
    $scope.login_details = Login.getProfile();
    $scope.data = {};
    $scope.selectedDate = null;
    $scope.data.selectedDateIndex = null;
    $scope.selectedTime = null;
    $scope.data.selectedTimeIndex = null;
    $scope.selectedNoTickets = 0;
    $scope.data.selectedNoTickets = null;
    $scope.currentFilter = 1;
    $scope.currentFilterName = 'current';
    $scope.totalCredits = [];
    $scope.totalCreditsNo = 0;
    $scope.alertPopup = null;
    $scope.today = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.currentHr = $filter('date')(new Date(), 'HH');
    $scope.currentMin = $filter('date')(new Date(), 'mm');
    // var d = '2017-09-05 20:20:00';
    // $scope.today = $filter('date')(new Date(d), 'yyyy-MM-dd');
    // $scope.currentHr = $filter('date')(new Date(d), 'HH');
    // $scope.currentMin = $filter('date')(new Date(d), 'mm');

    $scope.last_updated_time = moment().format('DD MMM YYYY HH:mm');

    $scope.timer = null;

    $scope.compareLessEqualTime = function(a, b, c) {
        var ampm = a.substring(a.length - 2, a.length);
        var time = a.substring(0, a.length - 3);
        var timesplit = time.split(":");
        var addhour = (ampm == "PM" ? 12 : 0);
        // console.log(ampm);
        // console.log(timesplit);
        // console.log(timesplit.length);
        // console.log(b);
        // console.log(c);
        // console.log(parseInt(timesplit[0], 10) + addhour);
        // console.log(parseInt(b, 10));
        // console.log(parseInt(timesplit[1], 10) + addhour);
        // console.log(parseInt(c, 10));
        if (timesplit.length == 2) {
            return ((parseInt(timesplit[0], 10) + addhour) < parseInt(b, 10)) ||
                ((parseInt(timesplit[0], 10) + addhour) == parseInt(b, 10) &&
                    parseInt(timesplit[1], 10) <= parseInt(c, 10));
        } else {
            return false;
        }
    };

    $scope.$on('$ionicView.leave', function() {
        if ($scope.timer) {
            $interval.cancel($scope.timer);
            $scope.timer = null;
        }
    });

    $scope.$on('$ionicView.enter', function() {
        $scope.ionsky = Login.ionsky;

        $scope.login_details = Login.getProfile();
        console.log($scope.login_details);
        // console.log(typeof($scope.login_details));
        // console.log(typeof($scope.login_details) === 'undefined');

        if (typeof($scope.login_details) === 'undefined') {
            if ($state.current.name == "app.ionskybook") {
                localStorage.setItem("last_login_view", "IONSkyBook");
            } else if ($state.current.name == "app.ionskywallet") {
                localStorage.setItem("last_login_view", "IONSkyWallet");
            }
            $scope.lastView = $ionicHistory.backView();
            $scope.redirect('backtoroot');
        } else if ($state.current.name == "app.ionskybook") {
            $scope.selectedDate = null;
            $scope.data.selectedDateIndex = null;
            $scope.selectedTime = null;
            $scope.data.selectedTimeIndex = null;
            $scope.selectedNoTickets = 0;
            $scope.data.selectedNoTickets = null;

            $scope.credits = $scope.groupTickets(IonSkyManager.credits);
            $scope.tickets = IonSkyManager.tickets;

            IonSkyManager.getCredits($scope.login_details.card_no, $scope.device_type, function($data, $status, $headers, $config) {
                console.log("Get Credits");
                $scope.credits = $scope.groupTickets($data.data.enovax.data);
                console.log($scope.credits);
            }, function($data, $status, $headers, $config) {
                var alertPopup = $ionicPopup.alert({
                    title: '',
                    template: $data.msg,
                    buttons: [{
                        type: 'ion-btn',
                        text: 'OK'
                    }]
                });
            });
            IonSkyManager.getTickets($scope.login_details.card_no, $scope.device_type, function($data, $status, $headers, $config) {
                console.log("Get Tickets");
                $scope.tickets = $data.data.enovax.data;
                // console.log($scope.tickets);
                // console.log($scope.tickets.data.eventGroups);
                // console.log($scope.tickets.data[0].eventGroups);
            }, function($data, $status, $headers, $config) {
                var alertPopup = $ionicPopup.alert({
                    title: '',
                    template: $data.msg,
                    buttons: [{
                        type: 'ion-btn',
                        text: 'OK'
                    }]
                });
            });
        } else if ($state.current.name == "app.ionskywallet") {
            $scope.refreshWallet();
        } else if ($state.current.name == "app.ionskyticketdetail") {
            $ionicLoading.show();
            $scope.refreshPass();

            $scope.timer = $interval(function() {
                $scope.refreshPass();
            }, 3000);
        }
    });

    $scope.groupTickets = function(tickets) {
        group = {};
        $scope.totalCredits = [];
        if (tickets) {
            for (var i = 0; i < tickets.data.length; i++) {
                var element = tickets.data[i];
                if (group[element.expiryDate] >= 1) {
                    group[element.expiryDate]++;
                } else {
                    group[element.expiryDate] = 1;
                }
                if ($scope.totalCredits.length <= 5) {
                    $scope.totalCredits.push($scope.totalCredits.length + 1);
                }
            }
        }

        $scope.totalCreditsNo = $scope.totalCredits.length;

        return group;
    }

    $scope.refreshWallet = function() {
        IonSkyManager.getPasses($scope.login_details.card_no, $scope.device_type, function($data, $status, $headers, $config) {
            $scope.last_updated_time = moment().format('DD MMM YYYY HH:mm');
            $scope.$broadcast('scroll.refreshComplete');
            console.log("Get Tickets");
            $scope.passes = $data.data.enovax.data;
            console.log($scope.passes);
        }, function($data, $status, $headers, $config) {
            $scope.$broadcast('scroll.refreshComplete');
            var alertPopup = $ionicPopup.alert({
                title: '',
                template: $data.msg,
                buttons: [{
                    type: 'ion-btn',
                    text: 'OK'
                }]
            });
        });
    }

    $scope.refreshPass = function() {
        passId = $stateParams.passId;
        if ($scope.login_details) {
            IonSkyManager.getPass(passId, $scope.login_details.card_no, $scope.device_type, function($data, $status, $headers, $config) {
                console.log("Get Pass");
                $scope.pass = $data.data.enovax.data.data[0];
                console.log($scope.pass);
                if ($scope.pass.freeQty <= 0) {
                    if ($scope.alertPopup == null) {
                        $scope.alertPopup = $ionicPopup.alert({
                            title: "",
                            template: "Ticket" + ($scope.pass.noOfCredits > 1 ? "s" : "") + " redeemed successfully",
                            buttons: [{
                                type: 'ion-btn',
                                text: 'OK'
                            }]
                        });
                        $scope.alertPopup.then(function(res) {
                            $interval(function() {
                                $state.go('app.dashboard');
                            }, 500, 1);
                        });
                    }
                } else if ($scope.pass.erroCode !== "") {
                    var key = passId + "-" + $scope.pass.errorTimestamp;
                    // localStorage.removeItem(key);
                    var lastHistory = localStorage.getItem(key);
                    console.log(lastHistory);
                    if (!lastHistory) {
                        localStorage.setItem(key, $scope.pass.errorTimestamp);
                        var msg = "There is an error during redemption. Please refer to staff: " + $scope.pass.erroCode;
                        if ($scope.pass.erroCode == "950") {
                            msg = "All the ticket has been redeemed.";
                        } else if ($scope.pass.erroCode == "951") {
                            msg = "Ticket booked is not allowed for entry yet. Please check your ticket details.";
                        } else if ($scope.pass.erroCode == "952") {
                            msg = "Ticket booked has expired. Please check your ticket details.";
                        } else if ($scope.pass.erroCode == "500") {
                            msg = "Invalid ticket. Please check your ticket details.";
                        }
                        if (!$scope.alertPopup) {
                            $scope.alertPopup = $ionicPopup.alert({
                                title: "",
                                template: msg,
                                buttons: [{
                                    type: 'ion-btn',
                                    text: 'OK'
                                }]
                            });
                            $scope.alertPopup.then(function(res) {
                                $scope.alertPopup = null;
                                $scope.redirect('IONSkyWallet');
                            });
                        }
                    }
                }

            }, function($data, $status, $headers, $config) {
                var alertPopup = $ionicPopup.alert({
                    title: '',
                    template: $data.msg,
                    buttons: [{
                        type: 'ion-btn',
                        text: 'OK'
                    }]
                });
            });
        } else {
            $scope.redirect('backtoroot');
        }
    }

    $scope.changeMenu = function(m) {
        $scope.currentFilter1 = (m == 'current') ? 1 : 2;
        $scope.currentFilter2 = (m == 'current') ? 1 : 3;
        $scope.currentFilterName = m;
    }

    $scope.filterHistory = function(history) {
        return history.status == $scope.currentFilter1 || history.status == $scope.currentFilter2;
    }

    $scope.loadDate = function() {
        console.log("Load Date");
        console.log($scope.data.selectedDateIndex);
        $scope.selectedDate = $scope.tickets.data[0].eventGroups[$scope.data.selectedDateIndex];
    };

    $scope.loadTime = function() {
        console.log("Load Time");
        console.log($scope.data.selectedTimeIndex);
        $scope.selectedTime = $scope.selectedDate.lines[$scope.data.selectedTimeIndex];
        var currentCredit = $scope.totalCreditsNo;
        if ($scope.selectedTime.capacity < $scope.totalCredits.length) {
            currentCredit = $scope.selectedTime.capacity;
        }
        console.log(currentCredit);
        $scope.totalCredits = [];
        for (var i = 0; i < currentCredit && $scope.totalCredits.length < 5; i++) {
            $scope.totalCredits.push($scope.totalCredits.length + 1);
        }
    };

    $scope.loadNoTicket = function() {
        console.log("Load NoTicket");
        console.log($scope.data.selectedNoTickets);
        $scope.selectedNoTickets = $scope.data.selectedNoTickets;
    };


    $scope.bookTickets = function() {
        if ($scope.selectedNoTickets > $scope.selectedTime.capacity) {
            var alertPopup = $ionicPopup.alert({
                title: '',
                template: "The number of tickets selected is more than the show capacity.",
                buttons: [{
                    type: 'ion-btn',
                    text: 'OK'
                }]
            });
        } else {
            IonSkyManager.book($scope.login_details.card_no, $scope.device_type,
                $scope.tickets.data[0].code, $scope.selectedNoTickets, $scope.selectedDate.eventGroupId,
                $scope.selectedTime.eventLineId,
                function($data, $status, $headers, $config) {
                    console.log("Book Tickets Success");
                    console.log($data);
                    if ($data.data.enovax.data.code === "200") {
                        pass = $data.data.enovax.data.data[0];
                        console.log(pass);
                        $state.go('app.ionskyticketdetail', { passId: pass.tikcetPassReference });
                    } else {
                        var msg = "General Application Error.";
                        switch ($data.data.enovax.data.code) {
                            case "920":
                                msg = "Check out failed.";
                                break;
                            case "921":
                                msg = "Invalid ticket credit reference.";
                                break;
                            case "922":
                                msg = "You are booking more than allowed quantity.";
                                break;
                            case "923":
                                msg = "The show is full.";
                                break;
                            case "924":
                                msg = "The ticket allocated has expired.";
                                break;
                            case "925":
                                msg = "The event date is wrong.";
                                break;
                            case "930":
                                msg = "Invalid access token.";
                                break;
                            default:
                                break;
                        }
                        var alertPopup = $ionicPopup.alert({
                            title: '',
                            template: msg,
                            buttons: [{
                                type: 'ion-btn',
                                text: 'OK'
                            }]
                        });
                    }
                },
                function($data, $status, $headers, $config) {
                    var alertPopup = $ionicPopup.alert({
                        title: '',
                        template: $data.msg,
                        buttons: [{
                            type: 'ion-btn',
                            text: 'OK'
                        }]
                    });
                });
        }
    };

    $scope.showDetail = function(pass) {
        if (pass.status == 1) {
            $state.go('app.ionskyticketdetail', { passId: pass.tikcetPassReference });
        }
    };

    $scope.skyRedirect = function(location) {
        $interval(function() {
            $scope.redirect(location);
        }, 500, 1);
    }
})